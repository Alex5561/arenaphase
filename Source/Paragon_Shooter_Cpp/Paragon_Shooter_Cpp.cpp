// Copyright Epic Games, Inc. All Rights Reserved.

#include "Paragon_Shooter_Cpp.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Paragon_Shooter_Cpp, "Paragon_Shooter_Cpp" );
