// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BulletHitIntarface.h"
#include "Explosive.generated.h"

UCLASS()
class PARAGON_SHOOTER_CPP_API AExplosive : public AActor, public IBulletHitIntarface
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AExplosive();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ImpactHit", meta = (AllowPrivateAccess = "true"))
		class UParticleSystem* ExplosionParticles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ImpactHit", meta = (AllowPrivateAccess = "true"))
		class USoundCue* ExplosionSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ImpactHit", meta = (AllowPrivateAccess = "true"))
		class USphereComponent* CollisionDamageSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ImpactHit", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* ExplosiveMesh;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void BulletHit_Implementation(FHitResult HitResut, AActor* Shooter, AController* InstigatorC) override;
};
