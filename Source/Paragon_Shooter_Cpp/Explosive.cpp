// Fill out your copyright notice in the Description page of Project Settings.


#include "Explosive.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/Character.h"

// Sets default values
AExplosive::AExplosive()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ExplosiveMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	SetRootComponent(ExplosiveMesh);

	CollisionDamageSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collsion Damage"));
	CollisionDamageSphere->SetupAttachment(GetRootComponent());

}

// Called when the game starts or when spawned
void AExplosive::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AExplosive::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AExplosive::BulletHit_Implementation(FHitResult HitResut, AActor* Shooter, AController* InstigatorC)
{
	if (ExplosionSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSound, GetActorLocation());
	}
	if (ExplosionParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionParticles, HitResut.Location, FRotator(0.f), true);
	}

   TArray<AActor*> OverlappingActor;
   GetOverlappingActors(OverlappingActor,ACharacter::StaticClass());

   for (auto Actor : OverlappingActor)
   {
	   UGameplayStatics::ApplyDamage(Actor,100.f,InstigatorC,Shooter,UDamageType::StaticClass());
   }

	Destroy();
}

