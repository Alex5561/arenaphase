// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Particles/ParticleSystemComponent.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/KismetMathLibrary.h"
#include "EnemyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/SphereComponent.h"
#include "ShooterCharacter.h"
#include "Components/CapsuleComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/SkeletalMeshSocket.h"

// Sets default values
AEnemy::AEnemy() :
	Health(100.f),
	MaxHealth(100.f),
	HealthBarDisplayTime(4.f),
	bHitReact(true),
	HitReactTimeMin(0.5f),
	HitReactTimeMax(1.f),
	HitNumberDestroyTime(1.5f),
	bStunned(false),
	StunChance(0.5f),
	bInAttackRange(false),
	AttackLFast(TEXT("AttackLFast")),
	AttackRFast(TEXT("AttackRFast")),
	AttackL(TEXT("AttackL")),
	AttackR(TEXT("AttackR")),
	BaseDamage(20.f),
	LeftWeaponSocket(TEXT("FX_Trail_L_01")),
	RightWeaponSocket(TEXT("FX_Trail_R_01")),
	bCanAttack(true),
	AttackTime(1.f),
	bDying(false),
	DeathTime(4.f)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AgrosSphere = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	AgrosSphere->SetupAttachment(GetRootComponent());

	CombatRangeSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CombatSphere"));
	CombatRangeSphere->SetupAttachment(GetRootComponent());

	LeftWeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("LeftWeaponCollision"));
	LeftWeaponCollision->SetupAttachment(GetMesh(),FName(TEXT("LeftWeaponSocket")));

	RightWeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("RightWeaponCollision"));
	RightWeaponCollision->SetupAttachment(GetMesh(),FName(TEXT("RightWeaponSocket")));
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	AgrosSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnSphereOverlap);
	CombatRangeSphere->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::OnCombatSphereOverlap);
	CombatRangeSphere->OnComponentEndOverlap.AddDynamic(this,&AEnemy::OnCombatSphereEndOverlap);
	LeftWeaponCollision->OnComponentBeginOverlap.AddDynamic(this,&AEnemy::LeftCollisionOverlap);
	RightWeaponCollision->OnComponentBeginOverlap.AddDynamic(this, &AEnemy::RightCollisionOverlap);



	EnemyController = Cast<AEnemyAIController>(GetController());

	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("CanAttack")), bCanAttack);
	}


	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility,ECollisionResponse::ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera,ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);

	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LeftWeaponCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	LeftWeaponCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	LeftWeaponCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn,ECollisionResponse::ECR_Overlap);

	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightWeaponCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	RightWeaponCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	RightWeaponCollision->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);


	FVector WorldPatrolPoint = UKismetMathLibrary::TransformLocation(GetActorTransform(),PatrolPoint);
	FVector WorldPatrolPoint2 = UKismetMathLibrary::TransformLocation(GetActorTransform(), PatrolPoint2);

	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsVector(FName(TEXT("PatrolPoint")),WorldPatrolPoint);
		EnemyController->GetBlacboardComponent()->SetValueAsVector(FName(TEXT("PatrolPoint2")), WorldPatrolPoint2);
		if (BehaviorTree)
		{
			EnemyController->RunBehaviorTree(BehaviorTree);
		}
	}

}

void AEnemy::ShowHealthBar_Implementation()
{
	GetWorldTimerManager().ClearTimer(HealthBarTimerHandle);
	GetWorldTimerManager().SetTimer(HealthBarTimerHandle,this,&AEnemy::HideHealthBar,HealthBarDisplayTime);
}

void AEnemy::Dead()
{
	if(bDying) return;
	bDying = true;

	HideHealthBar();
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && MontageDeath)
	{
		AnimInstance->Montage_Play(MontageDeath);
	}

	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("Dead")),true);
		EnemyController->StopMovement();
	}
	
}

void AEnemy::PlayHitMontage(FName MontageSection, float PlayRate /*= 1.f*/)
{
	if (bHitReact)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(HitMontage, PlayRate);
			AnimInstance->Montage_JumpToSection(MontageSection, HitMontage);
		}
		bHitReact = false;
		const float HitReactTime{FMath::FRandRange(HitReactTimeMin,HitReactTimeMax)};
		GetWorldTimerManager().SetTimer(HitReactTimer,this,&AEnemy::ResetHitReactTimer,HitReactTime);
	}
	
}

void AEnemy::ResetHitReactTimer()
{
	bHitReact = true;
}

void AEnemy::StoreHitNNumber(UUserWidget* HitNumber, FVector Location)
{
	HitNumbers.Add(HitNumber,Location);

	FTimerHandle HitNumberTimer;
	FTimerDelegate HitNumberDelegat;
	HitNumberDelegat.BindUFunction(this, FName(TEXT("DestroyHitNumber")),HitNumber);
	GetWorld()->GetTimerManager().SetTimer(HitNumberTimer,HitNumberDelegat,HitNumberDestroyTime,false);
}

void AEnemy::DestroyHitNumber(UUserWidget* HitNumber)
{
	HitNumbers.Remove(HitNumber);
	HitNumber->RemoveFromParent();
}

void AEnemy::UpdateHitNumber()
{
	for (auto& HitPair : HitNumbers)
	{	
		UUserWidget* HitNumber{HitPair.Key};
		const FVector Location{HitPair.Value};

		FVector2D ScreenPosition;
		UGameplayStatics::ProjectWorldToScreen(GetWorld()->GetFirstPlayerController(),Location,ScreenPosition);
		HitNumber->SetPositionInViewport(ScreenPosition);
	}
}

void AEnemy::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor == nullptr) return;

	auto CharacterShooter = Cast<AShooterCharacter>(OtherActor);
	if (CharacterShooter)
	{
		if (EnemyController)
		{
			if (EnemyController->GetBlacboardComponent())
			{
				EnemyController->GetBlacboardComponent()->SetValueAsObject(FName(TEXT("Target")), CharacterShooter);
			}
			
		}
	}
		
}



void AEnemy::OnCombatSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor == nullptr) return;
	auto ShooterCharacter = Cast<AShooterCharacter>(OtherActor);
	if (ShooterCharacter)
	{
		bInAttackRange = true;
		if (EnemyController)
		{
			EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("InAttackRange")), bInAttackRange);
		}
	}
	
}

void AEnemy::OnCombatSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == nullptr) return;
	auto ShooterCharacter = Cast<AShooterCharacter>(OtherActor);
	if (ShooterCharacter)
	{
		bInAttackRange = false;
		if (EnemyController)
		{
			EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("InAttackRange")), bInAttackRange);
		}
	}
}

void AEnemy::SetStunned(bool bStunedvalue)
{
	bStunned = bStunedvalue;

	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("Stunned")),bStunned);
	}
}

void AEnemy::PlayAttackMonyage(FName Section, float PlayRate)
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && AttackMontage)
	{
		AnimInstance->Montage_Play(AttackMontage, PlayRate);
		AnimInstance->Montage_JumpToSection(Section, AttackMontage);
	}
	bCanAttack = false;
	GetWorldTimerManager().SetTimer(AttackWaitTimer,this,&AEnemy::ResetCanAttack,AttackTime);
	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("CanAttack")),bCanAttack);
	}
}

FName AEnemy::GetAttackSectionName()
{
	FName SectionName;
	const int32 Section{FMath::RandRange(1,4)};
	switch (Section)
	{
	case 1:
			SectionName = AttackLFast;
		break;
	case 2: 
		SectionName = AttackRFast;
		break;
	case 3: 
		SectionName = AttackL;
		break;
	case 4:
		SectionName = AttackR;
		break;
	}
	return SectionName;
}

void AEnemy::LeftCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if(OtherActor == nullptr) return;
	auto ShooterCharacter = Cast<AShooterCharacter>(OtherActor);
	if (ShooterCharacter)
	{
		DoDamage(ShooterCharacter);
		SpawnBlood(ShooterCharacter,LeftWeaponSocket);
		StunCharacter(ShooterCharacter);
	}
	
}

void AEnemy::RightCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == nullptr) return;
	auto ShooterCharacter = Cast<AShooterCharacter>(OtherActor);
	if (ShooterCharacter)
	{
		DoDamage(ShooterCharacter);
		SpawnBlood(ShooterCharacter, RightWeaponSocket);
		StunCharacter(ShooterCharacter);
	}
}

void AEnemy::SpawnBlood(AShooterCharacter* ShooterCharacter, FName SocketName)
{
	const USkeletalMeshSocket* TipSocket{ GetMesh()->GetSocketByName(SocketName) };
	if (TipSocket)
	{
		const FTransform SocketTransform{ TipSocket->GetSocketTransform(GetMesh()) };
		if (ShooterCharacter->GetBloodParticle())
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ShooterCharacter->GetBloodParticle(), SocketTransform);
		}
	}
}

void AEnemy::StunCharacter(AShooterCharacter* Victim)
{
	if (Victim)
	{
		const float Stun{FMath::FRandRange(0.f,1.f)};
		if (Stun <= Victim->GetStunChance())
		{
			Victim->Stun();
		}
	}
}

void AEnemy::ResetCanAttack()
{
	bCanAttack = true;
	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("CanAttack")), bCanAttack);
	}
}

void AEnemy::FinishDead()
{
	GetMesh()->bPauseAnims = true;

	GetWorldTimerManager().SetTimer(DeathTimer,this,&AEnemy::DestroyEnemy,DeathTime);
}

void AEnemy::DestroyEnemy()
{
	Destroy();
}

void AEnemy::ActivadeLeftWeapon()
{
	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AEnemy::DeactivateLeftWeapon()
{
	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}	

void AEnemy::ActivateRightWeapon()
{
	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}

void AEnemy::DeactivadeRightWeapon()
{
	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AEnemy::DoDamage(AShooterCharacter* ShooterCharacter)
{
	if (ShooterCharacter == nullptr) return;
	if (ShooterCharacter)
	{
		UGameplayStatics::ApplyDamage(ShooterCharacter, BaseDamage, EnemyController, this, UDamageType::StaticClass());
		if (ShooterCharacter->GetMeleeImpactSound())
		{
			UGameplayStatics::PlaySoundAtLocation(this, ShooterCharacter->GetMeleeImpactSound(), GetActorLocation());
		}
	}
}



// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	UpdateHitNumber();
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

float AEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (EnemyController)
	{
		EnemyController->GetBlacboardComponent()->SetValueAsObject(FName(TEXT("Target")),DamageCauser);
	}
	if (Health - Damage <= 0.f)
	{
		Health = 0.f;
		Dead();
	}
	else
	{
		Health -= Damage;
	}
	if (bDying) return Damage;

	ShowHealthBar();
	const float Stunned = FMath::FRandRange(0.f, 1.f);
	if (Stunned <= StunChance)
	{
		PlayHitMontage(FName(TEXT("HitReactFront")));
		SetStunned(true);
	}
	return Damage;
}

void AEnemy::BulletHit_Implementation(FHitResult HitResult,AActor* Shooter, AController* InstigatorC)
{
	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(),ImpactSound,GetActorLocation());
	}
	if (ImpactParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),ImpactParticles,HitResult.Location,FRotator(0.f),true);
	}

	
}	
	

