// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AmmoType.h"
#include "ShooterCharacter.generated.h"



UENUM(BlueprintType)
enum class ECombatState : uint8
{
	ECS_Unoccupied UMETA(DisplayName = "Unoccupied"),
	ECS_FireTimerInProgress UMETA(DisplayName = "FireTimerInProgress"),
	ECS_Reloading UMETA(DisplayName = "Reloading"),
	ECS_Equpped UMETA(DisplayName = "Equpped"),
	ECS_Stunned UMETA(DisplayName = "Stunned"),

	ECS_MAX UMETA(DisplayName = "ECS_MAX")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEquipWeaponDelegat,int32,CurrentSlotIndex,int32, NewSlotIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHighLightIconDelegat, int32, SlotIndex, bool, bStartAnimation);

USTRUCT(BlueprintType)
struct FInterpLocation
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		class USceneComponent* SceneComponent;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
		int32 ItemCount;


};

UCLASS()
class PARAGON_SHOOTER_CPP_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//MoveForward Function
	void MoveForward(float Value);

	//Move Right Function
	void MoveRight(float Value);

	//Mouse
	void LookTurn(float Value);
	void LookUP(float Value);

	//GamePad
	void LookAtTurnRate(float Value);
	void LookUpAtRate(float Value);
	void FireWeapon();
	bool GetbeamEndLocation(const FVector& MuzzleSocketLocation, FHitResult& OutHeatResult);

	//Set Aiming Button Pressed/Released
	void AimingButtonPressed();
	void AimingButtonReleased();

	void CameraInterpZoom(float DeltaTime);

	void SetLookRate();

	void CalculateCrossHairSpread(float DeltaTime);

	UFUNCTION()
	void FinishCrossHairBooletTimer();

	void StartCrossHaitBulletFire();

	void FireButtonPressed();
	void FireButtonReleased();

	void StartFireTimer();
	UFUNCTION()
	void AutoFireReset();

	bool TraceUnderCrossHair(FHitResult& HitResult, FVector& OutHitLocation);

	void TraceForItems();

	class AWeapon* SpawnDefaultWeapon();

	void EquipWeapon(AWeapon* WeapontoEquip, bool bSwapping = false);

	void DropWeapon();

	void SelectButtonPressed();
	void SelectButtonReleased();
	void SwapWeapon(AWeapon* SwapWeapon);
	void InitilizeAmmoMap();
	bool WeaponHasAmmo();

	void PlayFireSound();

	void SendBullet();

	void PlayFireMontage();

	void ReloadButtonPressed();
	void ReloadWeapon();
	UFUNCTION(BlueprintCallable)
	void FinishReloading();
	bool CarryingAmmo();
	UFUNCTION(BlueprintCallable)
	void GrabClip();
	UFUNCTION(BlueprintCallable)
	void ReplaceClip();
	UFUNCTION(BlueprintCallable)
	void FinishEquipped();

	void CrouchButtonPressed();

	virtual void Jump() override;

	void InterpCapsuleHalfHeight(float DeltaTime);

	void Aim();
	void NoAim();

	void PickUpAmmo(class AAmmo* Value);

	void InitializeInterpLocation();
	
	void FKeyPressed();
	void OneKeyPressed();
	void TwoKeyPressed();
	void ThreeKeyPressed();
	void FourKeyPressed();
	void FiveKeyPreessed();

	void ExchangeInventoryItems(int32 CurrentItemIndex, int32 NewItemIndex);

	int32 GetEmptyInventorySlot();

	UFUNCTION()
	void HighligtInventorySlot();

	UFUNCTION(BlueprintCallable)
		EPhysicalSurface GetSurfaceTypeFootStep();

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable)
		void EndStun();

	void Dead();

	UFUNCTION(BlueprintCallable)
		void FinishDeath();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

private:
	/*Camera Boom Positioning behind the Character*/
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "Camera", meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		float BaseTurnRate;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		float BaseLookUpRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = " true"))
		class UAnimMontage* MontageFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = " true"))
		UParticleSystem* ImpactFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = " true"))
		UParticleSystem* BeamParticle;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		bool bAiming;

	//Interpol Camera Zoom Aiming
	float CameraDefaultFOV;

	float CameraZoomFOV;

	float CameraCurrentFOV;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = " true"))
	float ZoomInterpSpeed;

	//GamePad Speed Look Aiming/NoAiming
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float HipTurnRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float HipLookUpRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float AimingTurnRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float AimingLookUpRate;

	//Mouse Speed Look Aiming/NoAiming
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"),meta = (ClampMin = "0.0",ClampMax = "1.0"))
		float MouseHipTurnRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
		float MouseHipLookUpRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
		float MouseAimingTurnRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"), meta = (ClampMin = "0.0", ClampMax = "1.0"))
		float MouseAimingLookUpRate;

	//CrossHair Multiplay Factor
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHair", meta = (AllowPrivateAccess = "true"))
		float CroosHairSpreadMultiplay;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHair", meta = (AllowPrivateAccess = "true"))
		float CrossHairVelocityFactor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHair", meta = (AllowPrivateAccess = "true"))
		float CrossHairAimFactor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHair", meta = (AllowPrivateAccess = "true"))
		float CrossInAirFactor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHair", meta = (AllowPrivateAccess = "true"))
		float CrossHairShootingFactor;


	float ShootTimeDuration;
	bool bFiringBullet;
	FTimerHandle CroosHairShootTimer;

	bool bFireButtonPressed;
	bool bSholdFire;
	FTimerHandle AutoFireTimer;

	bool bShouldTraceForItems;
	int8 OverlappedItemCount;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Items",meta = (AllowPrivateAccess = "true"))
	class AItem* TraceHitItemLastFrame;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		 AWeapon* EquippedWeapon;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AWeapon>DefaultWeapon;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		AItem* TraceHitItem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		float CameraInterDistance;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		float CameraInterElevation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		TMap<EAmmoType, int32> AmmoMap;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		int32 Starting9MMAmmo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		int32 StartingARMAmmo;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "StateCharacter", meta = (AllowPrivateAccess = "true"))
		ECombatState CombatState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = " true"))
		UAnimMontage* ReloadMontage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = " true"))
		UAnimMontage* EquppedMontage;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "BoneClip", meta = (AllowPrivateAccess = "true"))
	FTransform TransformClip;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BoneClip", meta = (AllowPrivateAccess = "true"))
		USceneComponent* HandSceneComponent;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "Movement", meta = (AllowPrivateAccess = "true"))
		bool bCrouching;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "MovementSpeed",meta = (AllowPrivateAccess = "true"))
		float BaseMavementSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "MovementSpeed", meta = (AllowPrivateAccess = "true"))
		float CrouchMovementSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Capsule", meta = (AllowPrivateAccess = " true"))
	 float StandingCapsuleHalfHeight;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Capsule", meta = (AllowPrivateAccess = " true"))
	float CrouchingCapsuleHalfHeight;
	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category = "GroundFriction", meta = (AllowPrivateAccess = "true"))
		float BaseGroundFriction;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "GroundFriction", meta = (AllowPrivateAccess = "true"))
		float CrouchGroundFriction;

	bool bAimingButtonPressed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* WeaponInterpComp;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* InterpComp1;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* InterpComp2;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* InterpComp3;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* InterpComp4;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* InterpComp5;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "SceneInterp", meta = (AllowPrivateAccess = "true"))
		USceneComponent* InterpComp6;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "StructSceneArray", meta = (AllowPrivateAccess = "true"))
		TArray<FInterpLocation> InterpLocations;

	FTimerHandle PickUPSoundTimer;
	FTimerHandle EquipSoundTimer;

	bool bShouldPlayPickUPSound;
	bool bShouldPlayEquipSound;

	void ResetPickUPSoundTimer();
	void ResetEquipSoundTimer();

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category = "Items", meta = (AllowPrivateAccess = "true"))
	float PickUPSoundResetTime;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Items", meta = (AllowPrivateAccess = "true"))
	float EquipSoundResetTimer;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		TArray<AItem*>Inventory;

		const int32 INVENTORY_CAPACITY{6};
	
	UPROPERTY(BlueprintAssignable, Category = "Delegate", meta = (AllowPrivateAccess = "true"))
		FEquipWeaponDelegat EquipWeaponDelegat;
	UPROPERTY(BlueprintAssignable, Category = "Delegate", meta = (AllowPrivateAccess = "true"))
	FHighLightIconDelegat HighLightIconDelegat;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Inventroy" , meta = (AllowPrivateAccess = "true"))
		int32 HighkitedSlot;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Health", meta = (AllowPrivateAccess = "true"))
		float Health;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		float MaxHealth;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ImpactSound", meta = (AllowPrivateAccess = "true"))
		class USoundCue*  MeleeImpactSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ImpactBlood", meta = (AllowPrivateAccess = "true"))
		UParticleSystem* BloodParticles;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "HitReactStunMontage",meta = (AllowPrivateAccess = "true"))
		UAnimMontage* HitReactStun;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitReactStunPercent", meta = (AllowPrivateAccess = "true"))
		float StunChance;
	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "MontageDead", meta = (AllowPrivateAccess = "true"))
		UAnimMontage* MontageDeath;

public:

	/*Return CameraBoom Subobject*/
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	FORCEINLINE UCameraComponent* GetCameraComponent() const { return FollowCamera; }

	FORCEINLINE bool GetAiming() const { return bAiming; }

	UFUNCTION(BlueprintCallable)
	float GetCalculateCrossHairMultiplay() const;

	FORCEINLINE int8 GetOverlappedItemCount() const { return OverlappedItemCount; }

	void IncrementOverlappedItemCount(int8 Amount);

	FVector GetCameraIterpLocation();

	void GetPickUPItem(AItem* Item);

	FORCEINLINE ECombatState GetCombatState() const { return CombatState; }
	FORCEINLINE bool GetCrouching() const { return bCrouching; }
	FInterpLocation GetInterpLocation(int32 index);
	int32 GetInterpLocationIndex();
	void IncrementInterpLocationItemCount(int32 Index, int32 Amount);
	FORCEINLINE bool ShouldPlayPickUPSound() const { return bShouldPlayPickUPSound; }
	FORCEINLINE bool ShouldPlayEquippedSound() const { return bShouldPlayEquipSound; }

	void StartPickUPSoundTimer();
	void StartEquippedSoundTimer();
	UFUNCTION()
	void UnHighligtInventorySlot();

	FORCEINLINE AWeapon* GetEquippedWeapon() const {return EquippedWeapon;}
	FORCEINLINE USoundCue* GetMeleeImpactSound() const {return MeleeImpactSound;}
	FORCEINLINE UParticleSystem* GetBloodParticle() const {return BloodParticles;}

	void Stun();

	FORCEINLINE float GetStunChance() const {return StunChance;}
};
