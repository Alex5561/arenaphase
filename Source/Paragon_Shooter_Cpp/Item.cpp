// Fill out your copyright notice in the Description page of Project Settings.


#include "Item.h"
#include "Components/BoxComponent.h"
#include "Components/WidgetComponent.h"
#include "Components/SphereComponent.h"
#include "ShooterCharacter.h"
#include "Camera/CameraComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "Curves/CurveVector.h"

// Sets default values
AItem::AItem():
	ItemName(FString("Default")),
	ItemCount(0),
	ItemRarity(EItemRarity::EIR_Common),
	ItemState(EItemState::EIS_PickUP),
	ZCurveTime(0.7f),
	ItemInterStartLocation(FVector(0.f)),
	CameraTargetLocation(FVector(0.f)),
	bInterping(false),
	ItemInterX(0.f),
	ItemInterpY(0.f),
	InterpInitialYawOffset(0.f),
	ItemType(EItemType::EIT_Default),
	InterpLocIndex(1),
	MaterialIndexWeapon(0),
	bCanChangeCustomDepth(true),
	//DynamicMaterial
	GlowAmmount(150.f),
	FresnelExponet(3.f),
	FresnelReflectFraction(4.f),
	PulseCurveTime(5.f),
	SlotIndex(0),
	bCharacterInventoryFull(false)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ItemMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemMesh"));
	RootComponent = ItemMesh;

	CollisionBox = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionBox"));
	CollisionBox->SetupAttachment(RootComponent);
	CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);

	PickUpWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickUPWidget"));
	PickUpWidget->SetupAttachment(GetRootComponent());

	AreaSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere"));
	AreaSphere->SetupAttachment(GetRootComponent());
	AreaSphere->SetSphereRadius(150.f);
}

// Called when the game starts or when spawned
void AItem::BeginPlay()
{
	Super::BeginPlay();

	
	if (PickUpWidget)
	{
		PickUpWidget->SetVisibility(false);
	}
	
	SetActiveStars();

	AreaSphere->OnComponentBeginOverlap.AddDynamic(this,&AItem::OnSphereOverlap);
	AreaSphere->OnComponentEndOverlap.AddDynamic(this, &AItem::OnSphereEndOverlap);

	SetItemPropirties(ItemState);

	InitializeCustomDepth();
	
	StartPulseTimer();
}

void AItem::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(OtherActor);
		if (ShooterCharacter)
		{
			ShooterCharacter->IncrementOverlappedItemCount(1);
		}
	}
}

void AItem::OnSphereEndOverlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		AShooterCharacter* ShooterCharacter = Cast<AShooterCharacter>(OtherActor);
		if (ShooterCharacter)
		{
			ShooterCharacter->IncrementOverlappedItemCount(-1);
			ShooterCharacter->UnHighligtInventorySlot();
		}
	}
}

void AItem::SetActiveStars()
{
	for (int32 i = 0; i < 5; i++)
	{
		ActiveStars.Add(false);
	}
	switch(ItemRarity)
	{
	case EItemRarity::EIR_Damages: ActiveStars[1] = true;
		break;
	case EItemRarity::EIR_Common: ActiveStars[1] = true; ActiveStars[2] = true;
		break;
	case EItemRarity::EIR_UnCommon: ActiveStars[1] = true; ActiveStars[2] = true; ActiveStars[3] = true;
		break;
	case EItemRarity::EIR_Rare: ActiveStars[1] = true; ActiveStars[2] = true; ActiveStars[3] = true; ActiveStars[4] = true;
		break;
	case EItemRarity::EIR_Legendsry: ActiveStars[1] = true; ActiveStars[2] = true; ActiveStars[3] = true; ActiveStars[4] = true; ActiveStars[5] = true;
		break;
	}
}

void AItem::SetItemPropirties(EItemState State)
{
	switch (State)
	{
	case EItemState::EIS_PickUP:
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

		CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		CollisionBox->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);

		break;
	case EItemState::EIS_EquipInterping:
		PickUpWidget->SetVisibility(false);
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		break;
	case EItemState::EIS_PickedUP:
		PickUpWidget->SetVisibility(false);
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(false);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		break;
	case EItemState::EIS_Equipped:
		PickUpWidget->SetVisibility(false);
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		break;
	case EItemState::EIS_Falling:
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		ItemMesh->SetSimulatePhysics(true);
		ItemMesh->SetEnableGravity(true);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);

		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		CollisionBox->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		CollisionBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		break;
	case EItemState::EIS_MAX:

		break;
	default:
		break;
	}
}

void AItem::FinishInterping()
{
	bInterping = false;
	if (Character)
	{
		Character->IncrementInterpLocationItemCount(InterpLocIndex, -1);
		Character->GetPickUPItem(this);

		Character->UnHighligtInventorySlot();
	}
	SetActorScale3D(FVector(1.0f));
	DisabledGlowMaterial();
	bCanChangeCustomDepth = true;
	DisabledCustomDepth();
}

void AItem::ItemInterp(float DeltaTime)
{
	if (!bInterping) return;

	if (Character && ItemZCurve)
	{
		const float EllapsedTime = GetWorldTimerManager().GetTimerElapsed(ItemInterpTimer);
		const float CurveValue = ItemZCurve->GetFloatValue(EllapsedTime);
		FVector ItemLocation = ItemInterStartLocation;
		const FVector CameraInterpLocation{ GetInterpLocation() };
		const FVector ItemToCamera{ FVector(0.f,0.f,(CameraInterpLocation - ItemLocation).Z) };
		const float DeltaZ = ItemToCamera.Size();
		
		FVector CurrentLocation{ GetActorLocation() };
		const float InterpXValue = FMath::FInterpTo(CurrentLocation.X, CameraInterpLocation.X, DeltaTime, 30.0f);
		const float InterpYValue = FMath::FInterpTo(CurrentLocation.Y, CameraInterpLocation.Y, DeltaTime, 30.0f);

		ItemLocation.X = InterpXValue;
		ItemLocation.Y = InterpYValue;
		ItemLocation.Z += CurveValue * DeltaZ;
		SetActorLocation(ItemLocation, true, nullptr, ETeleportType::TeleportPhysics);


		const FRotator CameraRotation{ Character->GetCameraComponent()->GetComponentRotation() };
		FRotator ItemRotation{ 0.f, CameraRotation.Yaw  + InterpInitialYawOffset,0.f};
		SetActorRotation(ItemRotation, ETeleportType::TeleportPhysics);

		if (ItemScaleCurve)
		{
			const float ScaleCurveValue = ItemScaleCurve->GetFloatValue(EllapsedTime);
			SetActorScale3D(FVector(ScaleCurveValue, ScaleCurveValue, ScaleCurveValue));
		}
	}
}

FVector AItem::GetInterpLocation()
{
	if (Character == nullptr) return FVector(0.f);

	switch (ItemType)
	{
	case EItemType::EIT_Weapon: return Character->GetInterpLocation(0).SceneComponent->GetComponentLocation();
		break;
	case EItemType::EIT_Ammo : return  Character->GetInterpLocation(InterpLocIndex).SceneComponent->GetComponentLocation();
		break;
	}
	return FVector(0.f);
}

void AItem::PlayPickUPSound(bool bForsePlaySound)
{
	if (Character)
	{
		if (bForsePlaySound)
		{
			if (PickUpSound)
			{
				UGameplayStatics::PlaySound2D(this, PickUpSound);
			}
		}
		else if (Character->ShouldPlayPickUPSound())
		{
			Character->StartPickUPSoundTimer();
			if (PickUpSound)
			{
				UGameplayStatics::PlaySound2D(this, PickUpSound);
			}
		}
	}
}

void AItem::EnabledCustomDepth()
{
	if (bCanChangeCustomDepth)
	{
		ItemMesh->SetRenderCustomDepth(true);
	}
}

void AItem::DisabledCustomDepth()
{
	if (bCanChangeCustomDepth)
	{
		ItemMesh->SetRenderCustomDepth(false);
	}
	
}

void AItem::InitializeCustomDepth()
{
	DisabledCustomDepth();
}

void AItem::OnConstruction(const FTransform& Transform)
{
	if (MaterialInstance)
	{
		MaterialInstanceDynamic = UMaterialInstanceDynamic::Create(MaterialInstance, this);
		//MaterialInstanceDynamic->SetVectorParameterValue(TEXT("FresnelColor"), GlowColor);
		ItemMesh->SetMaterial(MaterialIndexWeapon, MaterialInstanceDynamic);
	}
	EnabledGlowMaterial();

	FString DataTableRarityPatch(TEXT("DataTable'/Game/_Game/DataTable/DT_ItemRarity.DT_ItemRarity'"));
	UDataTable* RarityTableObject = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(),nullptr,*DataTableRarityPatch));
	if (RarityTableObject)
	{
		FItemRarityTable* RarityRow = nullptr;
		switch (ItemRarity)
		{
		case EItemRarity::EIR_Damages:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName(TEXT("Damaged")),TEXT(""));
			break;
		case EItemRarity::EIR_Common:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName(TEXT("Common")), TEXT(""));
			break;
		case EItemRarity::EIR_UnCommon:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName(TEXT("Uncommon")), TEXT(""));
			break;
		case EItemRarity::EIR_Rare:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName(TEXT("Rare")), TEXT(""));
			break;
		case EItemRarity::EIR_Legendsry:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName(TEXT("Legendary")), TEXT(""));
			break;
		default:
			break;
		}

		if (RarityRow)
		{
			GlowColor = RarityRow->GlowColor;
			LightColor = RarityRow->LightColor;
			DarkColor = RarityRow->DarkColor;
			NumberOfStarts = RarityRow->NumberofStarts;
			IconBackground = RarityRow->IconBackground;
			if (GetItemMesh())
			{
				GetItemMesh()->SetCustomDepthStencilValue(RarityRow->CustomDepthStencelvalue);
			}
		}
	}

	
}

void AItem::EnabledGlowMaterial()
{
	if (MaterialInstanceDynamic)
	{
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("Glow_Alpha_Blend"), 0);
	}
}

void AItem::UpdatePulse()
{
	 float ElapsedTimer{};
	 FVector CurveValue{};
	switch (ItemState)
	{
	case EItemState::EIS_PickUP: 
		if (CurvePulse)
		{
			ElapsedTimer = GetWorldTimerManager().GetTimerElapsed(PulseTimer);
			CurveValue = CurvePulse->GetVectorValue(ElapsedTimer);
		}
		break;
	case EItemState::EIS_EquipInterping:
		if (InterpCurvePulse)
		{
			ElapsedTimer = GetWorldTimerManager().GetTimerElapsed(ItemInterpTimer);
			CurveValue = InterpCurvePulse->GetVectorValue(ElapsedTimer);
		}
		break;
	default:
		break;
	}
	if (MaterialInstanceDynamic)
	{
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("GlowAmount"), CurveValue.X * GlowAmmount);
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("FresnelExponentin"), CurveValue.Y * FresnelExponet);
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("FresnelReflectFraction"), CurveValue.Z * FresnelReflectFraction);
	}

}

void AItem::DisabledGlowMaterial()
{
	if (MaterialInstanceDynamic)
	{
		MaterialInstanceDynamic->SetScalarParameterValue(TEXT("Glow_Alpha_Blend"), 1);
	}
}

void AItem::PlayEquipSound(bool bForsePlaySound)
{
	if (Character)
	{
		if (bForsePlaySound)
		{
			if (EquippedSound)
			{
				UGameplayStatics::PlaySound2D(this, EquippedSound);
			}
		}
		else if (Character->ShouldPlayEquippedSound())
		{
			Character->StartEquippedSoundTimer();
			if (EquippedSound)
			{
				UGameplayStatics::PlaySound2D(this, EquippedSound);
			}
		}
	}
}

// Called every frame
void AItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	ItemInterp(DeltaTime);
	UpdatePulse();
}

void AItem::ResetPulseTimer()
{
	StartPulseTimer();
}

void AItem::StartPulseTimer()
{
	if (ItemState == EItemState::EIS_PickUP)
	{
		GetWorldTimerManager().SetTimer(PulseTimer, this, &AItem::ResetPulseTimer, PulseCurveTime);
	}
}

void AItem::SetItemState(EItemState State)
{
	ItemState = State;
	SetItemPropirties(State);
}

void AItem::StartItemCurve(AShooterCharacter* Char, bool bForseEquipSound)
{
	Character = Char;

	InterpLocIndex = Character->GetInterpLocationIndex();
	Character->IncrementInterpLocationItemCount(InterpLocIndex, 1);

	PlayPickUPSound(bForseEquipSound);
	ItemInterStartLocation = GetActorLocation();
	bInterping = true;
	SetItemState(EItemState::EIS_EquipInterping);
	GetWorldTimerManager().ClearTimer(PulseTimer);
	GetWorldTimerManager().SetTimer(ItemInterpTimer, this, &AItem::FinishInterping, ZCurveTime);
	const float CameraRotationYaw{ Character->GetCameraComponent()->GetComponentRotation().Yaw };
	const float ItemRotationYaw{ GetActorRotation().Yaw };
	InterpInitialYawOffset = ItemRotationYaw - CameraRotationYaw;
	bCanChangeCustomDepth = false;
}

