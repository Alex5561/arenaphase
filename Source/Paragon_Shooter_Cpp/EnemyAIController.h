// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

/**
 * 
 */
UCLASS()
class PARAGON_SHOOTER_CPP_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public:
	
	AEnemyAIController();

	virtual void OnPossess(APawn* InPawn) override;
	

private:

	UPROPERTY(BlueprintReadWrite, Category = "BlackBoard", meta = (AllowPrivateAccess = "true"))
	class UBlackboardComponent* BlackboardComponent;

	UPROPERTY(BlueprintReadWrite, Category = "BehaviorTree", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTreeComponent* BehaviorTreeComponent;
	
public:
	
	FORCEINLINE UBlackboardComponent* GetBlacboardComponent() const {return Blackboard;}
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorTreeComponent() const {return BehaviorTreeComponent;}

};
