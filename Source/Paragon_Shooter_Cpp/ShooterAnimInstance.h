// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "WeaponType.h"
#include "ShooterAnimInstance.generated.h"

UENUM(BlueprintType)
enum class EAimOffSetState : uint8
{
	EOS_Aiming UMETA(DisplayName = "EOS_Aiming"),
	EOS_Hip UMETA(DisplayName = "EOS_Hip"),
	EOS_Reloading UMETA(DisplayName = "EOS_Reloading"),
	EOS_Air UMETA(DisplayName = "EOS_Air"),

	EOS_MAX UMETA(DisplayName = "DefaultMax")

};



UCLASS()
class PARAGON_SHOOTER_CPP_API UShooterAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:

	UShooterAnimInstance();

	virtual void NativeInitializeAnimation() override;
	
	UFUNCTION(BlueprintCallable)
	void UpdateAnimationProperties(float DeltaTime);

	void TurnInPlace();

	void Lean(float DeltaTime);
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	class AShooterCharacter* ShooterCharacter;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	bool bIsInAir;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	bool bIsAccelerating;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float MovementOffSetYaw;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	float LastMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
	bool bAiming;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turn", meta = (AllowPrivateAccess = "true"))
	float CharacterYaw;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turn", meta = (AllowPrivateAccess = "true"))
	float CharacterYawLastframe;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "Turn",meta = (AllowPrivateAccess = "true"))
	float RootYawOffSet;

	float RotationCurve;
	float RotationCurveLastFrame;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Turn", meta = (AllowPrivateAccess = "true"))
	float Pitch;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly,Category = "Turn", meta = (AllowPrivateAccess = "true"))
	bool bReloading;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "EnumState", meta = (AllowPrivateAccess = "true"))
		EAimOffSetState AimOffSetSlate;

	
		FRotator CharacterRotationRun;

		FRotator CharacterRotationLastFrameRun;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "Run",meta = (AllowPrivateAccess = "true"))
		float YawDeltaRun;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		bool bCrouching;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "Recoil",meta = (AllowPrivateAccess = "true"))
		float RecoilWeight;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Recoil", meta = (AllowPrivateAccess = "true"))
		bool bTurningInPlace;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Recoil", meta = (AllowPrivateAccess = "true"))
	EWeaponType EquippedWeaponType;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Recoil", meta = (AllowPrivateAccess = "true"))
	bool bShouldUseFABRIK;
};
