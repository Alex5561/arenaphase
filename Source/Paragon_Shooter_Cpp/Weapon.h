// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "AmmoType.h"
#include "Engine/DataTable.h"
#include "WeaponType.h"
#include "Weapon.generated.h"



USTRUCT(BlueprintType)
struct FWeaponDataTable : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	EAmmoType AmmoType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 WeaponAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MagazingCapacity;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class USoundCue* PickupSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundCue* EquipSound;


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USkeletalMesh* ItemMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString ItemName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* InventoryIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* AmmoIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInstance* MaterialInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 MaterialIndexWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName ClipBoneNAme;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName ReloadMontageSection;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UAnimInstance> AnimInstance;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* CrossHairMiddle;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* CrossHairLeft;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* CrossHairRight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* CrossHairBottom;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* CrossHairTop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float AutoFireRate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USoundCue* FireSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FName BoneToHidden;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bAutomatic;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float HeadShotDamage;
		
};

UCLASS()
class PARAGON_SHOOTER_CPP_API AWeapon : public AItem
{
	GENERATED_BODY()
	
public:
	AWeapon();

	virtual void Tick(float DeltaTime) override;


protected:

	virtual void BeginPlay() override;
	void StopFalling();
	
	virtual void OnConstruction(const FTransform& Transform) override;

	void FinishMovingSlide();
	void UpdateSlideDisplacment();

private:
	FTimerHandle ThrowWeaponTimer;
	float ThrowWeaponTime;
	bool bIsFalling;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo", meta = (AllowPrivateAccess = "true"))
		int32 Ammo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo", meta = (AllowPrivateAccess = "true"))
		int32 MagazineCapacity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponType", meta = (AllowPrivateAccess = "true"))
		EWeaponType WeaponType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponType", meta = (AllowPrivateAccess = "true"))
		EAmmoType AmmoType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponReloadSection", meta = (AllowPrivateAccess = "true"))
		FName ReloadMontageSection;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "MovingClip",meta = (AllowPrivateAccess = "true"))
	bool bMovingClip;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "IndexBoneName", meta = (AllowPrivateAccess = "true"))
		FName ClipBoneName;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "DataTable", meta = (AllowPrivateAccess = "true"))
		class UDataTable* WeaponDataTable;

		int32 PreviousMaterialIndex;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly,Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			UTexture2D* CrossHairMiddle;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			UTexture2D* CrossHairLeft;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			UTexture2D* CrossHairRight;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			UTexture2D* CrossHairBottom;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			UTexture2D* CrossHairTop;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			float AutoFireRate;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			class UParticleSystem* MuzzleFlash;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			USoundCue* FireSound;
	
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "CrossHairTexture", meta = (AllowPrivateAccess = "true"))
			FName BoneToHidden;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pistol", meta = (AllowPrivateAccess = "true"))
			float SlideDisplacment;
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pistol", meta = (AllowPrivateAccess = "true"))
			UCurveFloat* SlideDisplacmentCurve;


			FTimerHandle HandleSlideTimer;

			float SlideDisplacmentTime;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pistol", meta = (AllowPrivateAccess = "true"))
			bool bMovingSlide;
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pistol", meta = (AllowPrivateAccess = "true"))
			float MaxSlideDisplacment;
		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Pistol", meta = (AllowPrivateAccess = "true"))
			float MaxRecoilRotation;
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Pistol", meta = (AllowPrivateAccess = "true"))
			float RecoilRotation;

		UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "FireWeapon", meta = (AllowPrivateAccess = "true"))
			bool bAutomatic;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireWeapon", meta = (AllowPrivateAccess = "true"))
		float Damage;
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireWeapon", meta = (AllowPrivateAccess = "true"))
		float HeadShotDamage;

public:

	void ThrowWeapon();

	FORCEINLINE int32 GetAmmo() const { return Ammo; }
	FORCEINLINE int32 GetMagazineCapacity() const { return MagazineCapacity; }
	void DecreatAmmo();
	FORCEINLINE EWeaponType GetWeaponType() const { return WeaponType; }
	FORCEINLINE EAmmoType GetAmmoType() const { return AmmoType; }
	FORCEINLINE FName GetMontsgeSection() const { return ReloadMontageSection; }
	FORCEINLINE void SetMontageSection(FName SectionMontage)  { ReloadMontageSection = SectionMontage; }
	void ReloadAmmo(int32 Amount);
	FORCEINLINE FName GetClipBoneName() const { return ClipBoneName; }
	FORCEINLINE void SetClipBoneName(FName BoneName)  {ClipBoneName = BoneName; }
	void SetMovingClip(bool Move) { bMovingClip = Move; }
	bool ClipFull();

	FORCEINLINE float GetAutoFireRate() const {return AutoFireRate;}
	FORCEINLINE UParticleSystem* GetMuzzleFlash() const {return MuzzleFlash;}
	FORCEINLINE USoundCue* GetFireSound() const {return FireSound;}
	FORCEINLINE bool GetAutomatic() const {return bAutomatic;}
	FORCEINLINE float GetDamage() const {return Damage;}
	FORCEINLINE float GetHeadShotDamage() const {return HeadShotDamage;}
	void StartSlideTimer();
};
