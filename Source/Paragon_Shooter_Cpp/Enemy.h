// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "BulletHitIntarface.h"
#include "Enemy.generated.h"

UCLASS()
class PARAGON_SHOOTER_CPP_API AEnemy : public ACharacter, public IBulletHitIntarface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintNativeEvent)
	void ShowHealthBar();
	void ShowHealthBar_Implementation();

	UFUNCTION(BlueprintImplementableEvent)
	void HideHealthBar();

	void Dead();

	void PlayHitMontage(FName MontageSection, float PlayRate = 1.f);

	void ResetHitReactTimer();

	UFUNCTION(BlueprintCallable)
	void StoreHitNNumber(UUserWidget* HitNumber, FVector Location);

	UFUNCTION()
	void DestroyHitNumber(UUserWidget* HitNumber);

	void UpdateHitNumber();
	
	UFUNCTION()
		void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnCombatSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnCombatSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable)
		void SetStunned(bool bStunedvalue);

	UFUNCTION(BlueprintCallable)
		void PlayAttackMonyage(FName Section, float PlayRate);
	UFUNCTION(BlueprintPure)
		FName GetAttackSectionName();

	UFUNCTION()
		void LeftCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void RightCollisionOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

		UFUNCTION(BlueprintCallable)
		void ActivadeLeftWeapon();
		UFUNCTION(BlueprintCallable)
		void DeactivateLeftWeapon();
		UFUNCTION(BlueprintCallable)
		void ActivateRightWeapon();
		UFUNCTION(BlueprintCallable)
		void DeactivadeRightWeapon();

	void DoDamage(class AShooterCharacter* ShooterCharacter);
	void SpawnBlood(AShooterCharacter* ShooterCharacter, FName SocketName);

	void StunCharacter(AShooterCharacter* Victim);

	void ResetCanAttack();

	UFUNCTION(BlueprintCallable)
		void FinishDead();

	UFUNCTION()
		void DestroyEnemy();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "ImpactHit",meta = (AllowPrivateAccess = "true"))
	class UParticleSystem* ImpactParticles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ImpactHit", meta = (AllowPrivateAccess = "true"))
	class USoundCue* ImpactSound;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "Health", meta = (AllowPrivateAccess = "true"))
	float Health;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
	float MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "BoneHead", meta = (AllowPrivateAccess = "true"))
	FString HeadBoneName;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "TimeHralthBar", meta = (AllowPrivateAccess = "true"))
	float HealthBarDisplayTime;

	FTimerHandle HealthBarTimerHandle;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimMontage", meta = (AllowPrivateAccess = "true"))
		UAnimMontage* HitMontage;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimMontage", meta = (AllowPrivateAccess = "true"))
		UAnimMontage* AttackMontage;

	FTimerHandle HitReactTimer;

	bool bHitReact;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimMontageTime", meta = (AllowPrivateAccess = "true"))
	float HitReactTimeMin;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "AnimMontageTime", meta = (AllowPrivateAccess = "true"))
	float HitReactTimeMax;
	UPROPERTY(VisibleAnywhere, Category = "HitMap", meta = (AllowPrivateAccess = "true"))
	TMap<UUserWidget*,FVector > HitNumbers;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "HitMap", meta = (AllowPrivateAccess = "true"))
	float HitNumberDestroyTime;

	UPROPERTY(EditAnywhere, Category = "AI", meta = (AllowPrivateAccess = "true"))
	class UBehaviorTree* BehaviorTree;

	UPROPERTY(EditAnywhere,Category = "PatrolPoint",meta = (AllowPrivateAccess = "true",MakeEditWidget = "true"))
	FVector PatrolPoint;

	UPROPERTY(EditAnywhere, Category = "PatrolPoint", meta = (AllowPrivateAccess = "true", MakeEditWidget = "true"))
		FVector PatrolPoint2;

	class AEnemyAIController* EnemyController;

	UPROPERTY(EditAnywhere,BlueprintReadOnly,Category = "CollisionEnemy",meta = (AllowPrivateAccess = "true"))
		class USphereComponent* AgrosSphere;

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		bool bStunned;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		float StunChance;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		bool bInAttackRange;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		class USphereComponent* CombatRangeSphere;
	UPROPERTY(VisibleAnywhere,BlueprintReadWrite,Category = "CollisionWeapon", meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* LeftWeaponCollision;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "CollisionWeapon", meta = (AllowPrivateAccess = "true"))
		class UBoxComponent* RightWeaponCollision;

	FName AttackLFast;
	FName AttackRFast;
	FName AttackL;
	FName AttackR;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		float BaseDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		FName LeftWeaponSocket;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Combat", meta = (AllowPrivateAccess = "true"))
		FName RightWeaponSocket;

	UPROPERTY(VisibleAnywhere,Category = "AttackRate", meta = (AllowprivateAccess = "true"))
		bool bCanAttack;
	UPROPERTY(EditAnywhere, Category = "AttackRate", meta = (AllowprivateAccess = "true"))
		float AttackTime;

	FTimerHandle AttackWaitTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeathMontage", meta = (AllowPrivateAccess = "true"))
		UAnimMontage* MontageDeath;

		bool bDying;

		FTimerHandle DeathTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DeathTime", meta = (AllowPrivateAccess = "true"))
		float DeathTime;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void BulletHit_Implementation(FHitResult HitResult, AActor* Shooter, AController* InstigatorC) override;
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

	FORCEINLINE FString GetHeadBoneName() const {return HeadBoneName;}

	UFUNCTION(BlueprintImplementableEvent)
		void ShotHitNumber(int32 Damage, FVector HitLocation,bool bHeadShot);

	FORCEINLINE UBehaviorTree* GetBehaviorTree() const {return BehaviorTree;}
 };
