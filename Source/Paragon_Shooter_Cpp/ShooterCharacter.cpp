// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/SkeletalMeshSocket.h"
#include "DrawDebugHelpers.h"
#include "Particles/ParticleSystemComponent.h"
#include "Item.h"
#include "Components/WidgetComponent.h"
#include "Weapon.h"
#include "Components/SphereComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Ammo.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Paragon_Shooter_Cpp.h"
#include "BulletHitIntarface.h"
#include "Enemy.h"
#include "EnemyAIController.h"
#include "BehaviorTree/BlackboardComponent.h"


// Sets default values
AShooterCharacter::AShooterCharacter() :
	BaseTurnRate(45.f),
	BaseLookUpRate(45.f),
	bAiming(false),
	CameraDefaultFOV(0.f),
	CameraZoomFOV(35.f),
	CameraCurrentFOV(0.f),
	ZoomInterpSpeed(20.f),
	HipTurnRate(90.f),
	HipLookUpRate(90.f),
	AimingTurnRate(20.f),
	AimingLookUpRate(20.f),
	MouseHipTurnRate(1.f),
	MouseHipLookUpRate(1.f),
	MouseAimingTurnRate(0.2f),
	MouseAimingLookUpRate(0.2f),
	CroosHairSpreadMultiplay(0.f),
	CrossHairVelocityFactor(0.f),
	CrossInAirFactor(0.f),
	CrossHairAimFactor(0.f),
	CrossHairShootingFactor(0.f),
	ShootTimeDuration(0.05f),
	bFiringBullet(false),
	bSholdFire(true),
	bFireButtonPressed(false),
	bShouldTraceForItems(false),
	OverlappedItemCount(0),
	CameraInterDistance(250.f),
	CameraInterElevation(65.f),
	Starting9MMAmmo(85),
	StartingARMAmmo(120),
	CombatState(ECombatState::ECS_Unoccupied),
	bCrouching(false),
	BaseMavementSpeed(650.f),
	CrouchMovementSpeed(300.f),
	StandingCapsuleHalfHeight(88.f),
	CrouchingCapsuleHalfHeight(44.f),
	BaseGroundFriction(2.f),
	CrouchGroundFriction(100.f),
	bAimingButtonPressed(false),
	bShouldPlayPickUPSound(true),
	bShouldPlayEquipSound(true),
	PickUPSoundResetTime(0.2f),
	EquipSoundResetTimer(0.2f),
	HighkitedSlot(-1),
	Health(100.f),
	MaxHealth(100.f),
	StunChance(0.25f)

{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	/* Create camera Boom*/
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("Camera Boom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.f;//Set Camera Length
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->SocketOffset = FVector(0.f, 50.f, 115.f);

	//Create FollowCamera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("Follow Camera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = true;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	HandSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HandScene"));

	WeaponInterpComp = CreateDefaultSubobject<USceneComponent>(TEXT("WeaponInterpComponent"));
	WeaponInterpComp->SetupAttachment(GetCameraComponent());

	InterpComp1 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpComp1"));
	InterpComp1->SetupAttachment(GetCameraComponent());

	InterpComp2 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpComp2"));
	InterpComp2->SetupAttachment(GetCameraComponent());

	InterpComp3 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpComp3"));
	InterpComp3->SetupAttachment(GetCameraComponent());

	InterpComp4 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpComp4"));
	InterpComp4->SetupAttachment(GetCameraComponent());

	InterpComp5 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpComp5"));
	InterpComp5->SetupAttachment(GetCameraComponent());

	InterpComp6 = CreateDefaultSubobject<USceneComponent>(TEXT("InterpComp6"));
	InterpComp6->SetupAttachment(GetCameraComponent());
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	if (FollowCamera)
	{
		CameraDefaultFOV = GetCameraComponent()->FieldOfView;
		CameraCurrentFOV = CameraDefaultFOV;
	}
	EquipWeapon(SpawnDefaultWeapon());
	Inventory.Add(EquippedWeapon);
	EquippedWeapon->SetSlotIndex(0);
	EquippedWeapon->DisabledCustomDepth();
	EquippedWeapon->DisabledGlowMaterial();
	EquippedWeapon->SetCharacter(this);
	InitilizeAmmoMap();
	GetCharacterMovement()->MaxWalkSpeed = BaseMavementSpeed;
	InitializeInterpLocation();
}

void AShooterCharacter::MoveForward(float Value)
{
	if ((Controller != 0) && (Value != 0))
	{
		const FRotator Rotation{ Controller->GetControlRotation() };
		const FRotator RotationYaw(0, Rotation.Yaw, 0);

		const FVector DIrection{ FRotationMatrix{RotationYaw}.GetUnitAxis(EAxis::X) };
		AddMovementInput(DIrection, Value);
	}
}

void AShooterCharacter::MoveRight(float Value)
{
	if ((Controller != 0) && (Value != 0))
	{
		const FRotator Rotation{ Controller->GetControlRotation() };
		const FRotator RotationYaw(0, Rotation.Yaw, 0);

		const FVector DIrection{ FRotationMatrix{RotationYaw}.GetUnitAxis(EAxis::Y) };
		AddMovementInput(DIrection, Value);
	}
}

void AShooterCharacter::LookTurn(float Value)
{
	float TurnScaleFactor{};
	if (bAiming)
	{
		TurnScaleFactor = MouseAimingTurnRate;
	}
	else
	{
		TurnScaleFactor = MouseHipTurnRate;
	}
	AddControllerYawInput(Value * TurnScaleFactor);
}

void AShooterCharacter::LookUP(float Value)
{
	float LookScaleFactor{};
	if (bAiming)
	{
		LookScaleFactor = MouseAimingLookUpRate;
	}
	else
	{
		LookScaleFactor = MouseHipLookUpRate;
	}
	AddControllerPitchInput(Value * LookScaleFactor);
}

void AShooterCharacter::LookAtTurnRate(float Value)
{
	AddControllerYawInput(Value * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookUpAtRate(float Value)
{
	AddControllerPitchInput(Value * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::FireWeapon()
{
	if (EquippedWeapon == nullptr) return;
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	//UE_LOG(LogTemp, Warning, TEXT("FireWeapon"));

	if (WeaponHasAmmo())
	{
		PlayFireSound();
		SendBullet();
		PlayFireMontage();
		StartCrossHaitBulletFire();
		EquippedWeapon->DecreatAmmo();
		StartFireTimer();

		if (EquippedWeapon->GetWeaponType() == EWeaponType::EWT_Pistol)
		{

		}
	}
}

bool AShooterCharacter::GetbeamEndLocation(const FVector& MuzzleSocketLocation, FHitResult& OutHeatResult)
{
	FVector OutBeamEndLocation;
	FHitResult CrossHairResult;
	bool bCrossHairHit = TraceUnderCrossHair(CrossHairResult, OutBeamEndLocation);

	if (bCrossHairHit)
	{
		OutBeamEndLocation = CrossHairResult.Location;
	}
	else
	{
		//OutBeamLocation
	}

	
	const FVector WeaponTraceStart{ MuzzleSocketLocation };
	const FVector StartoEnd{ OutBeamEndLocation - MuzzleSocketLocation };
	const FVector WeaponTraceEnd{ MuzzleSocketLocation + StartoEnd * 1.25f };
	GetWorld()->LineTraceSingleByChannel(OutHeatResult, WeaponTraceStart, WeaponTraceEnd, ECollisionChannel::ECC_Visibility);
	if (!OutHeatResult.bBlockingHit)
	{
		OutHeatResult.Location = OutBeamEndLocation;
		return false;
	}
	return true;
}

void AShooterCharacter::AimingButtonPressed()
{
	bAimingButtonPressed = true;
	if (CombatState != ECombatState::ECS_Reloading && CombatState !=ECombatState::ECS_Equpped && CombatState != ECombatState::ECS_Stunned)
	{
		Aim();
	}
}

void AShooterCharacter::AimingButtonReleased()
{
	bAimingButtonPressed = false;
	NoAim();
}

void AShooterCharacter::CameraInterpZoom(float DeltaTime)
{
	if (bAiming)
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraZoomFOV, DeltaTime, ZoomInterpSpeed);
	}
	else
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraDefaultFOV, DeltaTime, ZoomInterpSpeed);
	}
	GetCameraComponent()->SetFieldOfView(CameraCurrentFOV);
}

void AShooterCharacter::SetLookRate()
{
	if (bAiming)
	{
		BaseTurnRate = AimingTurnRate;
		BaseLookUpRate = AimingLookUpRate;
	}
	else
	{
		BaseTurnRate = HipTurnRate;
		BaseLookUpRate = HipLookUpRate;
	}
}

void AShooterCharacter::CalculateCrossHairSpread(float DeltaTime)
{
	FVector2D WalkSpeedRange{ 0.f,600.f };
	FVector2D VelocityMultiplayRange{ 0.f, 1.f };
	FVector Velocity{ GetVelocity() };
	Velocity.Z = 0.f;

	CrossHairVelocityFactor = FMath::GetMappedRangeValueClamped(WalkSpeedRange, VelocityMultiplayRange, Velocity.Size());

	if(GetCharacterMovement()->IsFalling())
	{
		CrossInAirFactor = FMath::FInterpTo(CrossInAirFactor, 2.25f, DeltaTime, 2.25f);
	}
	else
	{
		CrossInAirFactor = FMath::FInterpTo(CrossInAirFactor, 0.f, DeltaTime, 30.f);
	}

	if (bAiming)
	{
		CrossHairAimFactor = FMath::FInterpTo(CrossHairAimFactor, 0.6f, DeltaTime, 30.f);
	}
	else
	{
		CrossHairAimFactor = FMath::FInterpTo(CrossHairAimFactor, 0.0f, DeltaTime, 30.f);
	}

	if (bFiringBullet)
	{
		CrossHairShootingFactor = FMath::FInterpTo(CrossHairShootingFactor, 0.3f, DeltaTime, 60.f);
	}
	else
	{
		CrossHairShootingFactor = FMath::FInterpTo(CrossHairShootingFactor, 0.f, DeltaTime, 60.f);
	}

	CroosHairSpreadMultiplay = 0.5f + CrossHairVelocityFactor + CrossInAirFactor - CrossHairAimFactor + CrossHairShootingFactor;
}



void AShooterCharacter::FinishCrossHairBooletTimer()
{
	bFiringBullet = false;
	GetWorldTimerManager().ClearTimer(CroosHairShootTimer);
}



void AShooterCharacter::StartCrossHaitBulletFire()
{
	bFiringBullet = true;
	GetWorldTimerManager().SetTimer(CroosHairShootTimer, this, &AShooterCharacter::FinishCrossHairBooletTimer,ShootTimeDuration);
}

void AShooterCharacter::FireButtonPressed()
{
	bFireButtonPressed = true;
	FireWeapon();
}

void AShooterCharacter::FireButtonReleased()
{
	bFireButtonPressed = false;
}

void AShooterCharacter::StartFireTimer()
{
	if(EquippedWeapon == nullptr) return;
	CombatState = ECombatState::ECS_FireTimerInProgress;
	GetWorldTimerManager().SetTimer(AutoFireTimer, this, &AShooterCharacter::AutoFireReset, EquippedWeapon->GetAutoFireRate());
	
}

void AShooterCharacter::AutoFireReset()
{
	if(CombatState == ECombatState::ECS_Stunned) return;

	CombatState = ECombatState::ECS_Unoccupied;
	if(EquippedWeapon == nullptr) return;
	if(WeaponHasAmmo())
	{
		if (bFireButtonPressed && EquippedWeapon->GetAutomatic())
		{
			FireWeapon();
		}
	}
	else
	{
		ReloadWeapon();
	}
}

bool AShooterCharacter::TraceUnderCrossHair(FHitResult& HitResult, FVector& OutHitLocation)
{
	FVector2D Viemport;
	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(Viemport);
	}
	FVector2D CrossHairLocation(Viemport.X / 2.f, Viemport.Y / 2.f);
	FVector CrossHairWorldPosition;
	FVector CrossHairWorldDirection;

	bool bScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(UGameplayStatics::GetPlayerController(this, 0), CrossHairLocation, CrossHairWorldPosition, CrossHairWorldDirection);
	
	if (bScreenToWorld)
	{
		FVector Start{ CrossHairWorldPosition };
		FVector End{ Start + CrossHairWorldDirection * 5'000 };
		OutHitLocation = End;
		GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Visibility);
		if (HitResult.bBlockingHit)
		{
			OutHitLocation = HitResult.Location;
			return true;
		}
	}

	return false;
}

void AShooterCharacter::TraceForItems()
{
	if (bShouldTraceForItems)
	{
		FHitResult HitResult;
		FVector OutHitVector;
		TraceUnderCrossHair(HitResult, OutHitVector);
		if (HitResult.bBlockingHit)
		{
			TraceHitItem = Cast<AItem>(HitResult.Actor);
			const auto TraceHitWeapon = Cast<AWeapon>(TraceHitItem);
			if (TraceHitItem)
			{
				if (HighkitedSlot == -1)
				{
					HighligtInventorySlot();
				}
			}
			else
			{
				if (HighkitedSlot != -1)
				{
					UnHighligtInventorySlot();
				}
			}
			if (TraceHitItem && TraceHitItem->GetItemState() == EItemState::EIS_EquipInterping)
			{
				TraceHitItem = nullptr;
			}

			if (TraceHitItem && TraceHitItem->GetPickUPWidget())
			{
				TraceHitItem->GetPickUPWidget()->SetVisibility(true);
				TraceHitItem->EnabledCustomDepth();

				if (Inventory.Num() >= INVENTORY_CAPACITY)
				{
					TraceHitItem->SetCharacterInventoryFull(true);
				}
				else
				{
					TraceHitItem->SetCharacterInventoryFull(false);
				}
			}
			if (TraceHitItemLastFrame)
			{
				if (TraceHitItem != TraceHitItemLastFrame)
				{
					TraceHitItemLastFrame->GetPickUPWidget()->SetVisibility(false);
					TraceHitItemLastFrame->DisabledCustomDepth();
				}
			}
			TraceHitItemLastFrame = TraceHitItem;
		}
	}
	else if (TraceHitItemLastFrame)
	{
		TraceHitItemLastFrame->GetPickUPWidget()->SetVisibility(false);
		TraceHitItemLastFrame->DisabledCustomDepth();
	}
}

AWeapon* AShooterCharacter::SpawnDefaultWeapon()
{
	if (DefaultWeapon)
	{
		return  GetWorld()->SpawnActor<AWeapon>(DefaultWeapon);

	}
	return nullptr;
}

void AShooterCharacter::EquipWeapon(AWeapon* WeapontoEquip, bool bSwapping)
{
	if (WeapontoEquip)
	{
		const USkeletalMeshSocket* WeaponSocket = GetMesh()->GetSocketByName(FName("WeaponSocket"));
		if (WeaponSocket)
		{
			WeaponSocket->AttachActor(WeapontoEquip, GetMesh());
		}

		if (EquippedWeapon == nullptr)
		{
			EquipWeaponDelegat.Broadcast(-1, WeapontoEquip->GetSlotIndex());
		}
		else if(!bSwapping)
		{
			EquipWeaponDelegat.Broadcast(EquippedWeapon->GetSlotIndex(),WeapontoEquip->GetSlotIndex());
		}
		EquippedWeapon = WeapontoEquip;
		EquippedWeapon->SetItemState(EItemState::EIS_Equipped);
	}
}

void AShooterCharacter::DropWeapon()
{
	if (EquippedWeapon)
	{
		FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld,true);
		EquippedWeapon->GetItemMesh()->DetachFromComponent(DetachmentTransformRules);

		EquippedWeapon->SetItemState(EItemState::EIS_Falling);
		EquippedWeapon->ThrowWeapon();
	}
}

void AShooterCharacter::SelectButtonPressed()
{
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	if (TraceHitItem)
	{
		TraceHitItem->StartItemCurve(this,true);
		TraceHitItem = nullptr;
	}
	
}

void AShooterCharacter::SelectButtonReleased()
{
	//
}

void AShooterCharacter::SwapWeapon(AWeapon* SwapWeapon)
{
	if (Inventory.Num() - 1 >= EquippedWeapon->GetSlotIndex())
	{
		Inventory[EquippedWeapon->GetSlotIndex()] =SwapWeapon;
		SwapWeapon->SetSlotIndex(EquippedWeapon->GetSlotIndex());
	}

	DropWeapon();
	EquipWeapon(SwapWeapon, true);
	TraceHitItem = nullptr;
	TraceHitItemLastFrame = nullptr;
}

void AShooterCharacter::InitilizeAmmoMap()
{
	AmmoMap.Add(EAmmoType::EAT_9MM, Starting9MMAmmo);
	AmmoMap.Add(EAmmoType::EAT_AR, StartingARMAmmo);
}

bool AShooterCharacter::WeaponHasAmmo()
{
	if (EquippedWeapon == nullptr) return false;
	return EquippedWeapon-> GetAmmo() >0;
}

void AShooterCharacter::PlayFireSound()
{
	if (EquippedWeapon->GetFireSound())
	{
		UGameplayStatics::PlaySound2D(this, EquippedWeapon->GetFireSound());
	}
}

void AShooterCharacter::SendBullet()
{
	const USkeletalMeshSocket* BarrelSocket = EquippedWeapon->GetItemMesh()->GetSocketByName("BarelSocket");
	if (BarrelSocket)
	{
		FTransform BarrelSocketTransform = BarrelSocket->GetSocketTransform(EquippedWeapon->GetItemMesh());
		if (EquippedWeapon->GetMuzzleFlash())
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), EquippedWeapon->GetMuzzleFlash(), BarrelSocketTransform);
		}

		FHitResult BeamHitResult;
		bool bBeamEnd = GetbeamEndLocation(BarrelSocketTransform.GetLocation(), BeamHitResult);
		if (bBeamEnd)
		{
			if (BeamHitResult.Actor.IsValid())
			{
				IBulletHitIntarface* BulletHitIntarface = Cast<IBulletHitIntarface>(BeamHitResult.Actor.Get());
				if (BulletHitIntarface)
				{
					BulletHitIntarface->BulletHit_Implementation(BeamHitResult, this, GetController());
				}

				class AEnemy* HitEnemy = Cast<AEnemy>(BeamHitResult.Actor.Get());
				if (HitEnemy)
				{
					int32 Damage{};
					if (BeamHitResult.BoneName.ToString() == HitEnemy->GetHeadBoneName())
					{
						Damage = EquippedWeapon->GetHeadShotDamage();
						UGameplayStatics::ApplyDamage(BeamHitResult.Actor.Get(), EquippedWeapon->GetHeadShotDamage(), GetController(), this, UDamageType::StaticClass());
						HitEnemy->ShotHitNumber(Damage, BeamHitResult.Location,true);
					}
					else
					{
						Damage = EquippedWeapon->GetDamage();
						UGameplayStatics::ApplyDamage(BeamHitResult.Actor.Get(), EquippedWeapon->GetDamage(), GetController(), this, UDamageType::StaticClass());
						HitEnemy->ShotHitNumber(Damage, BeamHitResult.Location,false);
					}
					
				}
			}
			else
			{
				if (ImpactFX)
				{
					UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactFX, BeamHitResult.Location);
				}
			}
			if (BeamParticle)
			{
				UParticleSystemComponent* Beam = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BeamParticle, BarrelSocketTransform);
				if (Beam)
				{
					Beam->SetVectorParameter(FName("Target"), BeamHitResult.Location);
				}
			}
		}
	}
}

void AShooterCharacter::PlayFireMontage()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && MontageFire)
	{
		AnimInstance->Montage_Play(MontageFire);
		AnimInstance->Montage_JumpToSection(FName("StartFire"));
	}
}

void AShooterCharacter::ReloadButtonPressed()
{
	ReloadWeapon();
}

void AShooterCharacter::ReloadWeapon()
{
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	
	if (EquippedWeapon == nullptr) return;

	if (CarryingAmmo() && !EquippedWeapon->ClipFull())
	{
		if (bAiming)
		{
			NoAim();
		}
		CombatState = ECombatState::ECS_Reloading;
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance && ReloadMontage)
		{
			AnimInstance->Montage_Play(ReloadMontage);
			AnimInstance->Montage_JumpToSection(EquippedWeapon->GetMontsgeSection());
		}
	}
}

void AShooterCharacter::FinishReloading()
{
	if(CombatState == ECombatState::ECS_Stunned) return;

	CombatState = ECombatState::ECS_Unoccupied;
	if (bAimingButtonPressed)
	{
		Aim();
	}
	if (EquippedWeapon == nullptr) return;
	const EAmmoType AmmoType{EquippedWeapon->GetAmmoType() };
	if (AmmoMap.Contains(AmmoType))
	{
		int32 CurriedAmmo = AmmoMap[AmmoType];
		const int32 MagEmptySpace = EquippedWeapon->GetMagazineCapacity() - EquippedWeapon->GetAmmo();
		if (MagEmptySpace > CurriedAmmo)
		{
			EquippedWeapon->ReloadAmmo(CurriedAmmo);
			CurriedAmmo = 0;
			AmmoMap.Add(AmmoType, CurriedAmmo);
		}
		else
		{
			EquippedWeapon->ReloadAmmo(MagEmptySpace);
			CurriedAmmo -= MagEmptySpace;
			AmmoMap.Add(AmmoType, CurriedAmmo);
		}
	}
}

bool AShooterCharacter::CarryingAmmo()
{
	if (EquippedWeapon == nullptr) return false;
	EAmmoType AmmoType = EquippedWeapon->GetAmmoType();
	if (AmmoMap.Contains(AmmoType))
	{
		return AmmoMap[AmmoType] > 0;
	}

	return false;
}

void AShooterCharacter::GrabClip()
{
	if (EquippedWeapon == nullptr) return;
	if (HandSceneComponent == nullptr) return;
	int32 ClipBoneIndex{ EquippedWeapon->GetItemMesh()->GetBoneIndex(EquippedWeapon->GetClipBoneName())};
	 TransformClip = EquippedWeapon->GetItemMesh()->GetBoneTransform(ClipBoneIndex);

	 FAttachmentTransformRules TransformRules(EAttachmentRule::KeepRelative,true);
	 HandSceneComponent->AttachToComponent(GetMesh(),TransformRules, FName(TEXT("Hand_L")));
	 HandSceneComponent->SetWorldTransform(TransformClip);

	 EquippedWeapon->SetMovingClip(true);
}

void AShooterCharacter::ReplaceClip()
{
	EquippedWeapon->SetMovingClip(false);
}


void AShooterCharacter::FinishEquipped()
{
	if(CombatState == ECombatState::ECS_Stunned) return;

	CombatState = ECombatState::ECS_Unoccupied;
	if (bAimingButtonPressed)
	{
		Aim();
	}
}

void AShooterCharacter::CrouchButtonPressed()
{
	if (!GetCharacterMovement()->IsFalling())
	{
		bCrouching = !bCrouching;

	}
	if (bCrouching)
	{
		GetCharacterMovement()->MaxWalkSpeed = CrouchMovementSpeed;
		GetCharacterMovement()->GroundFriction = CrouchGroundFriction;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseMavementSpeed;
		GetCharacterMovement()->GroundFriction = BaseGroundFriction;
	}
}

void AShooterCharacter::Jump()
{
	if (bCrouching)
	{
		bCrouching = false;
		GetCharacterMovement()->MaxWalkSpeed = BaseMavementSpeed;
	}
	else
	{
		ACharacter::Jump();
	}
}

void AShooterCharacter::InterpCapsuleHalfHeight(float DeltaTime)
{
	float TargetCapsuleHalfHeight{};
	if (bCrouching)
	{
		TargetCapsuleHalfHeight = StandingCapsuleHalfHeight;
	}
	else
	{
		TargetCapsuleHalfHeight = CrouchingCapsuleHalfHeight;
	}
	const float InterpHalfHeight{ FMath::FInterpTo(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),TargetCapsuleHalfHeight,DeltaTime,20.f) };
	const float DeltaCapsuleHalfHeight{ InterpHalfHeight - GetCapsuleComponent()->GetScaledCapsuleHalfHeight() };
	const FVector MeshOffSet{ 0.f,0.f, -DeltaCapsuleHalfHeight };
	GetMesh()->AddLocalOffset(MeshOffSet);
	GetCapsuleComponent()->SetCapsuleHalfHeight(InterpHalfHeight);
}

void AShooterCharacter::Aim()
{
	bAiming = true;
	GetCharacterMovement()->MaxWalkSpeed = CrouchMovementSpeed;
}

void AShooterCharacter::NoAim()
{
	bAiming = false;
	if (!bAiming)
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseMavementSpeed;
	}
}

void AShooterCharacter::PickUpAmmo(class AAmmo* Value)
{
	if (AmmoMap.Find(Value->GetAmmoType()))
	{
		int32 AmmoCount{ AmmoMap[Value->GetAmmoType()] };
		AmmoCount += Value->GetCountAmmo();
		AmmoMap.Add(Value->GetAmmoType(), AmmoCount);
	}
	if (EquippedWeapon->GetAmmoType() == Value->GetAmmoType())
	{
		if (EquippedWeapon->GetAmmo() == 0)
		{
			ReloadWeapon();
		}
	}
	Value->Destroy();
}

void AShooterCharacter::InitializeInterpLocation()
{
	FInterpLocation WeaponLocation{ WeaponInterpComp, 0 };
	InterpLocations.Add(WeaponLocation);

	FInterpLocation InterpLoc1{ InterpComp1,1 };
	InterpLocations.Add(InterpLoc1);

	FInterpLocation InterpLoc2{ InterpComp2,2};
	InterpLocations.Add(InterpLoc2);

	FInterpLocation InterpLoc3{ InterpComp3,3 };
	InterpLocations.Add(InterpLoc3);

	FInterpLocation InterpLoc4{ InterpComp4,4 };
	InterpLocations.Add(InterpLoc4);

	FInterpLocation InterpLoc5{ InterpComp5,5 };
	InterpLocations.Add(InterpLoc5);

	FInterpLocation InterpLoc6{ InterpComp6,6 };
	InterpLocations.Add(InterpLoc6);
}

void AShooterCharacter::FKeyPressed()
{
	if(EquippedWeapon->GetSlotIndex() == 0) return;
	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 0);
}

void AShooterCharacter::OneKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 1) return;
	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 1);
}

void AShooterCharacter::TwoKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 2) return;
	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 2);
}

void AShooterCharacter::ThreeKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 3) return;
	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 3);
}

void AShooterCharacter::FourKeyPressed()
{
	if (EquippedWeapon->GetSlotIndex() == 4) return;
	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 4);
}

void AShooterCharacter::FiveKeyPreessed()
{
	if (EquippedWeapon->GetSlotIndex() == 5) return;
	ExchangeInventoryItems(EquippedWeapon->GetSlotIndex(), 5);
}

void AShooterCharacter::ExchangeInventoryItems(int32 CurrentItemIndex, int32 NewItemIndex)
{
	//if((CurrentItemIndex == NewItemIndex) || (NewItemIndex >= Inventory.Num()) || (CombatState != ECombatState::ECS_Unoccupied)) return;

	const bool bCanExchangeItems = (CurrentItemIndex != NewItemIndex) && (NewItemIndex < Inventory.Num()) && (CombatState == ECombatState::ECS_Unoccupied || CombatState == ECombatState::ECS_Equpped);

	if (bCanExchangeItems)
	{
		if (bAiming)
		{
			NoAim();
		}
		
		auto OldEquippedWeapon = EquippedWeapon;
		auto NewWeapon = Cast<AWeapon>(Inventory[NewItemIndex]);
		EquipWeapon(NewWeapon);

		OldEquippedWeapon->SetItemState(EItemState::EIS_PickedUP);
		NewWeapon->SetItemState(EItemState::EIS_Equipped);

		CombatState = ECombatState::ECS_Equpped;

		UAnimInstance* AnimInstanceCharacter = GetMesh()->GetAnimInstance();
		if (AnimInstanceCharacter)
		{
			AnimInstanceCharacter->Montage_Play(EquppedMontage, 1.f);
			AnimInstanceCharacter->Montage_JumpToSection(FName("Equip"));
		}
		NewWeapon->PlayEquipSound(true);
	}
	
}

int32 AShooterCharacter::GetEmptyInventorySlot()
{
	for (int32 i = 0; i < Inventory.Num(); i++)
	{
		if (Inventory[i] == nullptr)
		{
			return i;
		}

	}
	if (Inventory.Num() < INVENTORY_CAPACITY)
	{
		return Inventory.Num();
	}
	return -1;
}

void AShooterCharacter::HighligtInventorySlot()
{
	const int32 EmptySlot{GetEmptyInventorySlot()};
	HighLightIconDelegat.Broadcast(EmptySlot,true);
	HighkitedSlot = EmptySlot;
}

EPhysicalSurface AShooterCharacter::GetSurfaceTypeFootStep()
{
	FHitResult HitResult;
	const FVector StartLocation{GetActorLocation()};
	const FVector EndLocation{StartLocation + FVector(0.f,0.f,-400.f)};
	FCollisionQueryParams QueryParams;
	QueryParams.bReturnPhysicalMaterial = true;
	GetWorld()->LineTraceSingleByChannel(HitResult,StartLocation,EndLocation,ECollisionChannel::ECC_Visibility, QueryParams);

	return UPhysicalMaterial::DetermineSurfaceType(HitResult.PhysMaterial.Get());
}

float AShooterCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (Health - Damage <= 0.f)
	{
		Health = 0.f;
		Dead();

		auto EnemyController = Cast<AEnemyAIController>(EventInstigator);
		if (EnemyController)
		{
			EnemyController->GetBlacboardComponent()->SetValueAsBool(FName(TEXT("EnemyDead")),true);
		}
	}
	else
	{
		Health-=Damage;
	}
	return Damage;
}

void AShooterCharacter::EndStun()
{
	CombatState = ECombatState::ECS_Unoccupied;

	if (bAimingButtonPressed)
	{
		Aim();
	}
}

void AShooterCharacter::Dead()
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && MontageDeath)
	{
		AnimInstance->Montage_Play(MontageDeath);
	}
}

void AShooterCharacter::FinishDeath()
{
	GetMesh()->bPauseAnims = true;
	APlayerController* PC = UGameplayStatics::GetPlayerController(this,0);
	if (PC)
	{
		DisableInput(PC);
	}
}

void AShooterCharacter::UnHighligtInventorySlot()
{
	HighLightIconDelegat.Broadcast(HighkitedSlot, false);
	HighkitedSlot = -1;
}

void AShooterCharacter::Stun()
{
	if(Health <= 0.f) return;

	CombatState = ECombatState::ECS_Stunned;

	if (HitReactStun)
	{
		UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(HitReactStun);
		}
	}
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	CameraInterpZoom(DeltaTime);

	SetLookRate();

	CalculateCrossHairSpread(DeltaTime);

	TraceForItems();

	InterpCapsuleHalfHeight(DeltaTime);
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);
	
	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this,&AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this,&AShooterCharacter::MoveRight);

	PlayerInputComponent->BindAxis(TEXT("TurnRate"), this, &AShooterCharacter::LookTurn);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUP);

	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &AShooterCharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Jump"), IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction(TEXT("FireButton"), IE_Pressed, this, &AShooterCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction(TEXT("FireButton"), IE_Released, this, &AShooterCharacter::FireButtonReleased);

	PlayerInputComponent->BindAction(TEXT("Aiming"), IE_Pressed, this, &AShooterCharacter::AimingButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Aiming"), IE_Released, this, &AShooterCharacter::AimingButtonReleased);

	PlayerInputComponent->BindAction(TEXT("Select"), IE_Pressed, this, &AShooterCharacter::SelectButtonPressed);
	PlayerInputComponent->BindAction(TEXT("Select"), IE_Released, this, &AShooterCharacter::SelectButtonReleased);

	PlayerInputComponent->BindAction(TEXT("Reload"), IE_Pressed, this, &AShooterCharacter::ReloadButtonPressed);

	PlayerInputComponent->BindAction(TEXT("Crouch"), IE_Pressed, this, &AShooterCharacter::CrouchButtonPressed);

	PlayerInputComponent->BindAction(TEXT("FKey"), IE_Pressed, this, &AShooterCharacter::FKeyPressed);

	PlayerInputComponent->BindAction(TEXT("1Key"), IE_Pressed, this, &AShooterCharacter::OneKeyPressed);

	PlayerInputComponent->BindAction(TEXT("2Key"), IE_Pressed, this, &AShooterCharacter::TwoKeyPressed);
	
	PlayerInputComponent->BindAction(TEXT("3Key"), IE_Pressed, this, &AShooterCharacter::ThreeKeyPressed);

	PlayerInputComponent->BindAction(TEXT("4Key"), IE_Pressed, this, &AShooterCharacter::FourKeyPressed);

	PlayerInputComponent->BindAction(TEXT("5Key"), IE_Pressed, this, &AShooterCharacter::FiveKeyPreessed);

}

void AShooterCharacter::ResetPickUPSoundTimer()
{
	bShouldPlayPickUPSound = true;
}

void AShooterCharacter::ResetEquipSoundTimer()
{
	bShouldPlayEquipSound = true;
}

float AShooterCharacter::GetCalculateCrossHairMultiplay() const
{
	return CroosHairSpreadMultiplay;
}

void AShooterCharacter::IncrementOverlappedItemCount(int8 Amount)
{
	if (OverlappedItemCount + Amount <= 0)
	{
		OverlappedItemCount = 0;
		bShouldTraceForItems = false;
	}
	else
	{
		OverlappedItemCount += Amount;
		bShouldTraceForItems = true;
	}
}

FVector AShooterCharacter::GetCameraIterpLocation()
{
	const FVector CameraWorldLocation{ FollowCamera->GetComponentLocation() };
	const FVector CameraForward{ FollowCamera->GetForwardVector() };
	return CameraWorldLocation + CameraForward * CameraInterDistance + FVector(0.f,0.f,0.f);
}

void AShooterCharacter::GetPickUPItem(AItem* Item)
{
	Item->PlayEquipSound();

	auto Weapon = Cast<AWeapon>(Item);
	if (Weapon)
	{
		if (Inventory.Num() < INVENTORY_CAPACITY)
		{
			Weapon->SetSlotIndex(Inventory.Num());
			Inventory.Add(Weapon);
			Weapon->SetItemState(EItemState::EIS_PickedUP);
		}
		else
		{
			SwapWeapon(Weapon);
		}

		
	}
	auto Ammo = Cast<AAmmo>(Item);
	if (Ammo)
	{
		PickUpAmmo(Ammo);
	}
}

FInterpLocation AShooterCharacter::GetInterpLocation(int32 index)
{
	if (index <= InterpLocations.Num())
	{
		return InterpLocations[index];
	}
	return FInterpLocation();
}

int32 AShooterCharacter::GetInterpLocationIndex()
{
	int32 LowestIndex = 1;
	int32 LowestCount = INT_MAX;

	for (int32 i = 1; i < InterpLocations.Num(); i++)
	{
		if (InterpLocations[i].ItemCount < LowestCount)
		{
			LowestIndex = i;
			LowestCount = InterpLocations[i].ItemCount;
		}
	}
	return LowestIndex;
}

void AShooterCharacter::IncrementInterpLocationItemCount(int32 Index, int32 Amount)
{
	if (Amount < -1 || Amount > 1) return;

	if (InterpLocations.Num() >= Index)
	{
		InterpLocations[Index].ItemCount += Amount;
	}
}

void AShooterCharacter::StartPickUPSoundTimer()
{
	bShouldPlayPickUPSound = false;
	GetWorldTimerManager().SetTimer(PickUPSoundTimer, this, &AShooterCharacter::ResetPickUPSoundTimer, PickUPSoundResetTime);
}

void AShooterCharacter::StartEquippedSoundTimer()
{
	bShouldPlayEquipSound = false;
	GetWorldTimerManager().SetTimer(EquipSoundTimer, this, &AShooterCharacter::ResetEquipSoundTimer, EquipSoundResetTimer);
}

