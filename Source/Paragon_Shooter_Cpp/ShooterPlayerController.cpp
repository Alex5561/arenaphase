// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"
#include "Blueprint/UserWidget.h"

AShooterPlayerController::AShooterPlayerController()
{

}

void AShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();
	if (HUDOverlay)
	{
		OverlayHud = CreateWidget<UUserWidget>(this, HUDOverlay);
		if (OverlayHud)
		{
			OverlayHud->AddToViewport();
			OverlayHud->SetVisibility(ESlateVisibility::Visible);
		}
	}
}
