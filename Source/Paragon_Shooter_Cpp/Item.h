// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Engine/DataTable.h"
#include "Item.generated.h"

UENUM(BlueprintType)
enum class EItemRarity :uint8
{
	EIR_Damages UMETA(DisplayName = "Damaged"),
	EIR_Common UMETA(DisplayName = "Common"),
	EIR_UnCommon UMETA(DisplayName = "UnCommon"),
	EIR_Rare UMETA(DisplayName = "Rare"),
	EIR_Legendsry UMETA(DisplayName = "Legendsry"),
	EIR_DefaultMAX UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EItemState : uint8
{
	EIS_PickUP UMETA(DisplayName = "PickUP"),
	EIS_EquipInterping UMETA(DisplayName = "EquipInterping"),
	EIS_PickedUP UMETA(DisplayName = "PickedUP"),
	EIS_Equipped UMETA(DisplayName = "Equipped"),
	EIS_Falling UMETA(DisplayName = "Falling"),



	EIS_MAX UMETA(DisplayName = "DefaultMAX")
};

UENUM(BlueprintType)
enum class EItemType : uint8
{
	EIT_Ammo UMETA(DisplayName = "Ammo"),
	EIT_Weapon UMETA(DisplayName = "Weapon"),

	EIT_Default UMETA(DisplayName = "Default")
};

USTRUCT(BlueprintType)
struct FItemRarityTable : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
	FLinearColor GlowColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor LightColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FLinearColor DarkColor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 NumberofStarts;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* IconBackground;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 CustomDepthStencelvalue;
};

UCLASS()
class PARAGON_SHOOTER_CPP_API AItem : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AItem();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent,AActor* OtherActor,UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,bool bFromSweep,const FHitResult& SweepResult);

	UFUNCTION()
		void OnSphereEndOverlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void SetActiveStars();

	virtual void SetItemPropirties(EItemState State);

	void FinishInterping();

	void ItemInterp(float DeltaTime);

	FVector GetInterpLocation();

	void PlayPickUPSound(bool bForsePlaySound = false);


	virtual void InitializeCustomDepth();

	virtual void OnConstruction(const FTransform& Transform)override;

	void EnabledGlowMaterial();

	void UpdatePulse();
	
	void StartPulseTimer();
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	virtual void EnabledCustomDepth();
	virtual void DisabledCustomDepth();

private:

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		 class UBoxComponent* CollisionBox;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		USkeletalMeshComponent* ItemMesh;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		class UWidgetComponent* PickUpWidget;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
		class USphereComponent* AreaSphere;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		FString ItemName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		int32 ItemCount;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Rarity", meta = (AllowPrivateAccess = "true"))
		EItemRarity ItemRarity;
	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		TArray<bool> ActiveStars;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		EItemState ItemState;

	//InterTimer
	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
	class UCurveFloat* ItemZCurve;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		FVector ItemInterStartLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		FVector CameraTargetLocation;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		bool bInterping;

	FTimerHandle ItemInterpTimer;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
	class AShooterCharacter* Character;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		float ZCurveTime;

	float ItemInterX;
	float ItemInterpY;

	float InterpInitialYawOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		class UCurveFloat* ItemScaleCurve;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		class USoundCue* PickUpSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Propirties", meta = (AllowPrivateAccess = "true"))
		class USoundCue* EquippedSound;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "ItemType",meta = (AllowPrivateAccess = "true"))
		EItemType ItemType;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ItemProperties", meta = (AllowPrivateAccess = "true"))
		int32 InterpLocIndex;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "MaterialIndex", meta = (AllowPrivateAccess = "true"))
	int32 MaterialIndexWeapon;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "MaterialIndex", meta = (AllowPrivateAccess = "true"))
	UMaterialInstanceDynamic* MaterialInstanceDynamic;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MaterialIndex", meta = (AllowPrivateAccess = "true"))
	UMaterialInstance* MaterialInstance;

	bool bCanChangeCustomDepth;

	UPROPERTY(EditDefaultsOnly,BlueprintReadOnly,Category = "Curve", meta = (AllowPrivateAccess = "true"))
	class UCurveVector* CurvePulse;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Curve", meta = (AllowPrivateAccess = "true"))
	UCurveVector* InterpCurvePulse;

	FTimerHandle PulseTimer;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Curve", meta = (AllowPrivateAccess = "true"))
	float PulseCurveTime;
	UPROPERTY(VisibleAnywhere,Category = "Curve", meta = (AllowPrivateAccess = "true"))
	float GlowAmmount;
	UPROPERTY(VisibleAnywhere, Category = "Curve", meta = (AllowPrivateAccess = "true"))
	float FresnelExponet;
	UPROPERTY(VisibleAnywhere, Category = "Curve", meta = (AllowPrivateAccess = "true"))
	float FresnelReflectFraction;

	void ResetPulseTimer();

	//UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "BackGroundImage", meta = (AllowPrivateAccess = "true"))
	//class UTexture2D* BackGroundImage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BackGroundImage", meta = (AllowPrivateAccess = "true"))
		class UTexture2D* IconItem;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "BackGroundImage", meta = (AllowPrivateAccess = "true"))
		class UTexture2D* AmmoIcon;

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	int32 SlotIndex;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
	bool bCharacterInventoryFull;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "DataTable", meta = (AllowPrivateAccess = "true"))
	class UDataTable* ItemRarityDataTable;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rarity", meta = (AllowPrivateAccess = "true"))
		FLinearColor GlowColor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rarity", meta = (AllowPrivateAccess = "true"))
		FLinearColor LightColor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rarity", meta = (AllowPrivateAccess = "true"))
		FLinearColor DarkColor;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rarity", meta = (AllowPrivateAccess = "true"))
		int32 NumberOfStarts;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Rarity", meta = (AllowPrivateAccess = "true"))
		UTexture2D* IconBackground;


public:

	FORCEINLINE UWidgetComponent* GetPickUPWidget() const { return PickUpWidget; }
	FORCEINLINE USphereComponent* GetSphereComponent() const { return AreaSphere; }
	FORCEINLINE UBoxComponent* GetCollisionBoxComponent() const { return CollisionBox; }
	FORCEINLINE EItemState GetItemState() const { return ItemState; }
	void SetItemState(EItemState State);
	FORCEINLINE USkeletalMeshComponent* GetItemMesh() const { return ItemMesh; }
	void StartItemCurve(AShooterCharacter* Char,bool bForseEquipSound = false);
	FORCEINLINE USoundCue* GetPickUPSound() const { return PickUpSound; }
	FORCEINLINE void SetPickUPSound(USoundCue* SoundPickUp) {PickUpSound = SoundPickUp;}
	FORCEINLINE USoundCue* GetEquippedSound() const { return EquippedSound; }
	FORCEINLINE void SetEquipSound(USoundCue* SoundEquip){EquippedSound = SoundEquip;}
	FORCEINLINE int32 GetCountAmmo() const { return ItemCount; }
	void PlayEquipSound(bool bForsePlaySound = false);
	void DisabledGlowMaterial();
	FORCEINLINE int32 GetSlotIndex() const {return SlotIndex;}
	void SetSlotIndex(int32 Index){SlotIndex = Index;}
	FORCEINLINE void SetCharacter(AShooterCharacter* Char) {Character = Char;}
	FORCEINLINE void SetCharacterInventoryFull(bool bFull){bCharacterInventoryFull = bFull;}
	FORCEINLINE void SetItemName(FString ItemNameValue) {ItemName = ItemNameValue;}
	FORCEINLINE void SetItemIcon(UTexture2D* ItemIconValue) { IconItem = ItemIconValue; }
	FORCEINLINE void SetAmmoIcon(UTexture2D* AmmoIconValue) { AmmoIcon = AmmoIconValue; }
	FORCEINLINE void SetMaterialInstance(UMaterialInstance* InstanceValue) {MaterialInstance = InstanceValue;}
	FORCEINLINE UMaterialInstance* GetMaterialInstance() const {return MaterialInstance;}
	FORCEINLINE void SetMaterialInstanceDynamic(UMaterialInstanceDynamic* InstanceDynamicValue) { MaterialInstanceDynamic = InstanceDynamicValue; }
	FORCEINLINE UMaterialInstanceDynamic* GetMaterialInstanceDynamic() const { return MaterialInstanceDynamic; }
	FORCEINLINE FLinearColor GetGlowColor() const {return GlowColor;}
	FORCEINLINE int32 GetMaterialIndex() const {return MaterialIndexWeapon;}
	FORCEINLINE void SetMaterialIndex(int32 IndexValue) {MaterialIndexWeapon = IndexValue;}
};
