// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterAnimInstance.h"
#include "ShooterCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Weapon.h"
#include "Kismet/KismetMathLibrary.h"


UShooterAnimInstance::UShooterAnimInstance() :
	Speed(0.f),
	bIsInAir(false),
	bIsAccelerating(false),
	MovementOffSetYaw(0.f),
	LastMovement(0.f),
	bAiming(false),
	CharacterYaw(0.f),
	CharacterYawLastframe(0.f),
	RootYawOffSet(0.f),
	Pitch(0.f),
	bReloading(false),
	AimOffSetSlate(EAimOffSetState::EOS_Hip),
	YawDeltaRun(0.f),
	CharacterRotationLastFrameRun(FRotator(0.f)),
	CharacterRotationRun(FRotator(0.f)),
	RecoilWeight(1.f),
	bTurningInPlace(false),
	EquippedWeaponType(EWeaponType::EWT_MAX),
	bShouldUseFABRIK(false)
{

}

void UShooterAnimInstance::UpdateAnimationProperties(float DeltaTime)
{
	if (ShooterCharacter == nullptr)
	{
		ShooterCharacter = Cast<AShooterCharacter>(TryGetPawnOwner());
	}
	if (ShooterCharacter)
	{
		bCrouching = ShooterCharacter->GetCrouching();
		bReloading = ShooterCharacter->GetCombatState() == ECombatState::ECS_Reloading;
		bShouldUseFABRIK = ShooterCharacter->GetCombatState() == ECombatState::ECS_Unoccupied || ShooterCharacter->GetCombatState() == ECombatState::ECS_FireTimerInProgress;
		//get Speed Character
		FVector Velocity{ ShooterCharacter->GetVelocity() };
		Velocity.Z = 0;
		Speed = Velocity.Size();

		// is Falling Character
		bIsInAir = ShooterCharacter->GetMovementComponent()->IsFalling();



		//Acceleration Character
		if (ShooterCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.f)
		{
			bIsAccelerating = true;
		}
		else
		{
			bIsAccelerating = false;
		}

		FRotator AimRotation = ShooterCharacter->GetBaseAimRotation();

		FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(ShooterCharacter->GetVelocity());

		MovementOffSetYaw = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, AimRotation).Yaw;
		
		if (ShooterCharacter->GetVelocity().Size() > 0.f)
		{
			LastMovement = MovementOffSetYaw;
		}

		bAiming = ShooterCharacter->GetAiming();

		if (bReloading)
		{
			AimOffSetSlate = EAimOffSetState::EOS_Reloading;
		}
		else if (bIsInAir)
		{
			AimOffSetSlate = EAimOffSetState::EOS_Air;
		}
		else if (ShooterCharacter->GetAiming())
		{
			AimOffSetSlate = EAimOffSetState::EOS_Aiming;
		}
		else
		{
			AimOffSetSlate = EAimOffSetState::EOS_Hip;
		}
		if (ShooterCharacter->GetEquippedWeapon())
		{
			EquippedWeaponType = ShooterCharacter->GetEquippedWeapon()->GetWeaponType();
		}
			
	}
	TurnInPlace();
	Lean(DeltaTime);
}

void UShooterAnimInstance::TurnInPlace()
{
	if (ShooterCharacter == nullptr) return;

	Pitch = ShooterCharacter->GetBaseAimRotation().Pitch;



	if (Speed > 0 || bIsInAir)
	{
		RootYawOffSet = 0.f;
		CharacterYaw = ShooterCharacter->GetActorRotation().Yaw;
		CharacterYawLastframe = CharacterYaw;
		RotationCurveLastFrame = 0.f;
		RotationCurve = 0.f;
	}
	else
	{
		CharacterYawLastframe = CharacterYaw;
		CharacterYaw = ShooterCharacter->GetActorRotation().Yaw;
		const float YawDelta{ CharacterYaw - CharacterYawLastframe };

		RootYawOffSet =UKismetMathLibrary::NormalizeAxis(RootYawOffSet - YawDelta);
		const float Turning{ GetCurveValue(TEXT("Turning")) };
		if (Turning > 0)
		{
			bTurningInPlace = true;
			RotationCurveLastFrame = RotationCurve;
			RotationCurve = GetCurveValue(TEXT("Rotation"));
			const float DeltaRotation{ RotationCurve - RotationCurveLastFrame };
			
			RootYawOffSet > 0 ? RootYawOffSet -= DeltaRotation : RootYawOffSet += DeltaRotation;

			const float ABSRootYawOffSet = FMath::Abs(RootYawOffSet);
			if (ABSRootYawOffSet > 90.f)
			{
				const float YawExcess{ ABSRootYawOffSet - 90.f };
				RootYawOffSet > 0 ? RootYawOffSet -= YawExcess : RootYawOffSet += YawExcess;
			}
		}
		else
		{
			bTurningInPlace = false;
			
		}
		
	}
	if (bTurningInPlace)
	{
		if (bReloading)
		{
			if (bCrouching)
			{
				RecoilWeight = 1.f;
			}
			else
			{
				RecoilWeight = 0.f;
			}
		}
		else
		{
			if (bCrouching)
			{
				if (bReloading)
				{
					RecoilWeight = 1.f;
				}
				else
				{
					RecoilWeight = 0.1f;
				}
			}
			else
			{
				if (bAiming || bReloading)
				{
					RecoilWeight = 1.f;
				}
				else
				{
					RecoilWeight = 0.5;
				}
			}
		}
	}
	else
	{
		RecoilWeight = 1.f;
	}
}



void UShooterAnimInstance::Lean(float DeltaTime)
{
	if (ShooterCharacter == nullptr) return;
	CharacterRotationLastFrameRun = CharacterRotationRun;
	CharacterRotationRun = ShooterCharacter->GetActorRotation();

	const FRotator DeltaRotation{ UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotationRun, CharacterRotationLastFrameRun) };
	const float Target{ DeltaRotation.Yaw / DeltaTime };

	const float Interp{ FMath::FInterpTo(YawDeltaRun,Target,DeltaTime,6.f) };

	YawDeltaRun = FMath::Clamp(Interp, -90.f, 90.f);
}

void UShooterAnimInstance::NativeInitializeAnimation()
{
	ShooterCharacter = Cast<AShooterCharacter>(TryGetPawnOwner());
}