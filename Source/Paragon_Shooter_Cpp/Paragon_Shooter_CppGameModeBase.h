// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Paragon_Shooter_CppGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class PARAGON_SHOOTER_CPP_API AParagon_Shooter_CppGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
