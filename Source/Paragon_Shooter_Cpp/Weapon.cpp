// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "Materials/MaterialInstanceDynamic.h"


AWeapon::AWeapon() :
	ThrowWeaponTime(0.7f),
	bIsFalling(false),
	Ammo(30),
	MagazineCapacity(30),
	WeaponType(EWeaponType::EWT_SubMachineGun),
	AmmoType(EAmmoType::EAT_9MM),
	ReloadMontageSection(FName(TEXT("ReloadSMG"))),
	ClipBoneName(TEXT("smg_clip")),
	SlideDisplacment(0.f),
	SlideDisplacmentTime(0.2f),
	bMovingSlide(false),
	MaxSlideDisplacment(4.f),
	MaxRecoilRotation(20.f),
	bAutomatic(true)
	
{
	PrimaryActorTick.bCanEverTick = true;
}

void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetItemState() == EItemState::EIS_Falling && bIsFalling)
	{
		const FRotator MeshRotation{ 0.f , GetItemMesh()->GetComponentRotation().Yaw, 0.f };
		GetItemMesh()->SetWorldRotation(MeshRotation, false, nullptr, ETeleportType::TeleportPhysics);
	}

	UpdateSlideDisplacment();
}

void AWeapon::ThrowWeapon()
{
	FRotator MeshRotation{ 0.f , GetItemMesh()->GetComponentRotation().Yaw, 0.f };
	GetItemMesh()->SetWorldRotation(MeshRotation, false, nullptr, ETeleportType::TeleportPhysics);
	const FVector MeshForward{ GetItemMesh()->GetForwardVector() };
	const FVector MeshRight{ GetItemMesh()->GetRightVector() };
	//FVector ImpulsDirection = MeshRight.RotateAngleAxis(-20.f, MeshForward);

	float RandomRotation{ 30.f };
	FVector ImpulsDirection = MeshRight.RotateAngleAxis(RandomRotation, FVector(0.f, 0.f, 1.f));
	ImpulsDirection *= 20'000.f;
	GetItemMesh()->AddImpulse(ImpulsDirection);

	bIsFalling = true;
	GetWorldTimerManager().SetTimer(ThrowWeaponTimer, this, &AWeapon::StopFalling, ThrowWeaponTime);
	EnabledGlowMaterial();
}

void AWeapon::DecreatAmmo()
{
	if (Ammo - 1 <= 0)
	{
		Ammo = 0;
	}
	else
	{
		--Ammo;
	}
}

void AWeapon::ReloadAmmo(int32 Amount)
{
	checkf(Ammo + Amount <= MagazineCapacity, TEXT("Magazine>"));
	Ammo += Amount;
}

bool AWeapon::ClipFull()
{
	return Ammo >= MagazineCapacity;
}

void AWeapon::StartSlideTimer()
{
	bMovingSlide = true;
	GetWorldTimerManager().SetTimer(HandleSlideTimer,this,&AWeapon::FinishMovingSlide,SlideDisplacmentTime);
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	if (BoneToHidden != FName(""))
	{
		GetItemMesh()->HideBoneByName(BoneToHidden, EPhysBodyOp::PBO_None);
	}
}

void AWeapon::StopFalling()
{
	bIsFalling = false;
	SetItemState(EItemState::EIS_PickUP);
	StartPulseTimer();
}

void AWeapon::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	const FString  WeaponDataTAblePatch{TEXT ("DataTable'/Game/_Game/DataTable/DT_WeaponDataTable.DT_WeaponDataTable'")};
	UDataTable* DataTableObjet = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(),nullptr,*WeaponDataTAblePatch));

	if (DataTableObjet)
	{
		FWeaponDataTable* WeaponTableRow = nullptr;
		switch (WeaponType)
		{
		case EWeaponType::EWT_SubMachineGun:
			WeaponTableRow = DataTableObjet->FindRow<FWeaponDataTable>(FName(TEXT("SubmachineGun")),TEXT(""));
			break;
		case EWeaponType::EWT_AssaultRifle:
			WeaponTableRow = DataTableObjet->FindRow<FWeaponDataTable>(FName(TEXT("AssaultRifle")), TEXT(""));
			break;
		case EWeaponType::EWT_Pistol:
			WeaponTableRow = DataTableObjet->FindRow<FWeaponDataTable>(FName(TEXT("Pistol")), TEXT(""));
			break;

		default:
			break;
		}
		if (WeaponTableRow)
		{
			AmmoType = WeaponTableRow->AmmoType;
			Ammo = WeaponTableRow->WeaponAmmo;
			MagazineCapacity = WeaponTableRow->MagazingCapacity;
			SetPickUPSound(WeaponTableRow->PickupSound);
			SetEquipSound(WeaponTableRow->EquipSound);
			GetItemMesh()->SetSkeletalMesh(WeaponTableRow->ItemMesh);
			SetItemName(WeaponTableRow->ItemName);
			SetItemIcon(WeaponTableRow->InventoryIcon);
			SetAmmoIcon(WeaponTableRow->AmmoIcon);
			SetMaterialInstance(WeaponTableRow->MaterialInstance);
			PreviousMaterialIndex = GetMaterialIndex();
			GetItemMesh()->SetMaterial(GetMaterialIndex(),nullptr);
			SetMaterialIndex(WeaponTableRow->MaterialIndexWeapon);
			SetClipBoneName(WeaponTableRow->ClipBoneNAme);
			SetMontageSection(WeaponTableRow->ReloadMontageSection);
			GetItemMesh()->SetAnimInstanceClass(WeaponTableRow->AnimInstance);
			CrossHairMiddle = WeaponTableRow->CrossHairMiddle;
			CrossHairBottom = WeaponTableRow->CrossHairBottom;
			CrossHairLeft = WeaponTableRow->CrossHairLeft;
			CrossHairRight = WeaponTableRow->CrossHairRight;
			CrossHairTop = WeaponTableRow->CrossHairTop;
			AutoFireRate = WeaponTableRow->AutoFireRate;
			MuzzleFlash = WeaponTableRow->MuzzleFlash;
			FireSound = WeaponTableRow->FireSound;
			BoneToHidden = WeaponTableRow->BoneToHidden;
			bAutomatic = WeaponTableRow->bAutomatic;
			Damage = WeaponTableRow->Damage;
			HeadShotDamage = WeaponTableRow->HeadShotDamage;
			
			if (GetMaterialInstance())
			{
				SetMaterialInstanceDynamic(UMaterialInstanceDynamic::Create(GetMaterialInstance(), this));
				GetMaterialInstanceDynamic()->SetVectorParameterValue(TEXT("FresnelColor"), GetGlowColor());
				GetItemMesh()->SetMaterial(GetMaterialIndex(), GetMaterialInstanceDynamic());

				EnabledGlowMaterial();
			}
			
		}
	}
	
	

}

void AWeapon::FinishMovingSlide()
{
	bMovingSlide =false;
}

void AWeapon::UpdateSlideDisplacment()
{
	if (SlideDisplacmentCurve && bMovingSlide)
	{
		const float ElapsedTime {GetWorldTimerManager().GetTimerElapsed(HandleSlideTimer)};
		const float CurveValue {SlideDisplacmentCurve->GetFloatValue(ElapsedTime)};
		SlideDisplacment = CurveValue * MaxSlideDisplacment;
		RecoilRotation = CurveValue * MaxRecoilRotation;
	}
}

